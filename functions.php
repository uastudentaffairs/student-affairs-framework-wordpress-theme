<?php

// Get directory
global $sa_framework_dir;
$sa_framework_dir = trailingslashit( get_template_directory_uri() );

// Get the slug
global $sa_framework_site_slug;
$sa_framework_site_slug = ( $sa_framework_site_slug_field = get_option( 'options_sa_framework_site_slug' ) ) ? preg_replace( '/[^a-z0-9]/i', '-', $sa_framework_site_slug_field ) : get_sa_subdomain_slug();

// Add admin functionality in admin
if( is_admin() ) {
	require_once( TEMPLATEPATH . '/includes/admin.php' );
}

// Include files
require_once( TEMPLATEPATH . '/includes/filters.php' );
require_once( TEMPLATEPATH . '/includes/shortcodes.php' );
require_once( TEMPLATEPATH . '/includes/search.php' );
require_once( TEMPLATEPATH . '/includes/walker-nav-menu.php' );

// Add theme support
add_action( 'after_setup_theme', function() {
	add_theme_support( 'post-thumbnails' );
});

// Add excerpts to pages
add_action( 'init', function() {
	add_post_type_support( 'page', 'excerpt' );
});

// Add image sizes
add_action( 'after_setup_theme', function() {

	add_image_size( 'sa-facebook', 1200, 628, true );
	add_image_size( 'sa-twitter', 1024, 512, true );

}, 10 );

// Register menus
register_nav_menus( array(
	'sa-main'           => 'Main Menu',
	'sa-search-results' => 'Search Results',
	'sa-404'            => '404 Page',
));

// Is this the main Student Affairs site?
function is_sa_main_site() {

	// Will be true if cache variable was found
	$is_main_site_found = false;
	$is_main_site = wp_cache_get( 'is_main_site', 'sa_framework', false, $is_main_site_found );

	// If not found, then set cache
	if ( ! $is_main_site_found ) {
		$is_main_site = is_multisite() && is_main_site();
		wp_cache_set( 'is_main_site', $is_main_site, 'sa_framework' );
	}

	return $is_main_site;
}

// After WP object is set up, set up a few template variables
add_action( 'wp', function() {
	global $post;

	// Get the ID
	$page_section_post_id = false;
	if ( isset( $post ) && isset( $post->ID ) ) {

		// Set page section
		$page_section_post_id = $post->ID;

		// Set current post parent ID
		$post_parent_id = isset( $post->post_parent ) ? $post->post_parent : 0;

		// If the current post has a parent, loop through the parents
		while ( $post_parent_id > 0 ) {

			// Get the parent post
			if ( $post_parent = get_post( $post_parent_id ) ) {

				// If this post has a parent, keep the loop going
				if ( $post_parent->post_parent > 0 ) {
					$post_parent_id = $post_parent->post_parent;
				}

				// Otherwise, set this post as the page section and close the loop
				else {
					$page_section_post_id = $post_parent->ID;
					break;
				}

			}

			// This means we couldn't find the post which shouldn't happen so get out of here
			else {
				break;
			}

		}

	}

	// Filter the ID
	$page_section_post_id = apply_filters( 'sa_framework_page_section_post_id', $page_section_post_id );
	
	// Set the ID
	if ( $page_section_post_id ) {

		// Set the post ID
		wp_cache_set( 'page_section_post_id', $page_section_post_id, 'sa_framework' );

	}

	// Get the display main menu setting - true by default
	$display_main_menu = strcasecmp( 'no', get_option( 'options_sa_framework_display_main_menu' ) ) == 0 ? false : true;

	// Filter the "display main menu" setting
	$display_main_menu = apply_filters( 'sa_framework_display_main_menu', $display_main_menu );

	// Make sure it's either true or false
	if ( $display_main_menu !== true && $display_main_menu !== false ) {
		$display_main_menu = true;
	}

	// Store the 'display_main_menu' setting
	wp_cache_set( 'display_main_menu', $display_main_menu, 'sa_framework' );

	// Get the display off canvas main menu setting - by default will match $display_main_menu
	$display_off_canvas_main_menu = $display_main_menu ? ( strcasecmp( 'no', get_option( 'options_sa_framework_display_off_canvas_main_menu' ) ) == 0 ? false : true ) : $display_main_menu;

	// Filter the "display off canvas main menu" setting
	if ( $display_main_menu ) {
		$display_off_canvas_main_menu = apply_filters( 'sa_framework_display_off_canvas_main_menu', $display_off_canvas_main_menu );
	}

	// Make sure it's either true or false
	if ( $display_off_canvas_main_menu !== true && $display_off_canvas_main_menu !== false ) {
		$display_off_canvas_main_menu = true;
	}

	// Store the 'display_off_canvas_main_menu' setting
	wp_cache_set( 'display_off_canvas_main_menu', $display_off_canvas_main_menu, 'sa_framework' );

});

// Set the page layout - has to run after 'template_include' to pick up IS_SA_SEARCH_PAGE
add_action( 'wp_head', function() {
	global $post;

	// Get this page's defined layout
	$defined_page_layout = null;

	// Make sure it has a post to check
	if ( isset( $post ) && isset( $post->ID ) ) {

		// Get the page layout from settings
		$defined_page_layout = is_front_page() ? get_option( 'options_sa_framework_default_front_page_layout' ) : get_post_meta( $post->ID, 'sa_framework_page_layout', true );

	}

	// If no defined layout, get the set default layout - default is '1 Full Width Column'
	if ( ! $defined_page_layout || strcasecmp( 'default', $defined_page_layout ) == 0 ) {
		$defined_page_layout = get_option( 'options_sa_framework_default_page_layout' );
	}

	// Set the page layout in cache
	wp_cache_set( 'page_layout', apply_filters( 'sa_framework_page_layout', $defined_page_layout ), 'sa_framework' );

});

//! Setup styles and scripts
add_action( 'wp_enqueue_scripts', function () {
	global $sa_framework_dir;

	// Load Fonts
	// @TODO Check on fonts to make sure we're not loading unnecessary fonts
	wp_enqueue_style( 'sa-fonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Roboto:400,300,500,700|Open+Sans:300,400,600,700' );

	// If no child theme, enqueue the base framework styles
	if ( ! is_child_theme() ) {
		wp_enqueue_style( 'sa-framework', $sa_framework_dir . 'css/sa-framework.min.css', array( 'sa-fonts' ) );
	}

	// Enqueue the base print styles
	wp_enqueue_style( 'sa-framework-print', $sa_framework_dir . 'css/sa-framework-print.min.css', array( 'sa-fonts' ), false, 'print' );

    // Enqueue modernizr - goes in header
    wp_enqueue_script( 'modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js' );

	// Register the jQuery UI because we need the easings
	wp_register_script( 'jquery-ui-effects-core', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js', array( 'jquery' ), false, true );

	// Register our script
	wp_enqueue_script( 'sa-framework', $sa_framework_dir . 'js/sa-framework.min.js', array( 'jquery', 'jquery-ui-effects-core' ), false, true );


	// Enqueue styles and script for off canvas menu
	if ( wp_cache_get( 'display_off_canvas_main_menu', 'sa_framework' ) ) {
		wp_enqueue_style( 'sa-framework-off-canvas-menu', $sa_framework_dir . 'css/sa-framework-off-canvas-menu.min.css' );
		wp_enqueue_script( 'sa-framework-off-canvas-menu', $sa_framework_dir . 'js/sa-framework-off-canvas-menu.min.js', array( 'jquery', 'sa-framework' ), false, true );
	}

	// If we're to show the grid lines
	// If settings say to show the grid lines...
	if ( current_user_can( 'manage_options' ) && ( $display_grid_lines = get_option( 'options_sa_framework_display_grid_lines' ) ) ) {
		wp_enqueue_style( 'sa-framework-grid-lines', $sa_framework_dir . 'css/sa-framework-grid-lines.min.css', array(), null );
	}

}, 10 );

//! Add grid lines to the footer
add_action( 'wp_footer', function() {

	// If settings say to show the grid lines...
	if ( current_user_can( 'manage_options' ) && ( $display_grid_lines = get_option( 'options_sa_framework_display_grid_lines' ) ) ) {

		// If we're to show the grid lines
		?><div id="sa-framework-grid-lines">
			<div class="row"><?php

				for ( $c = 1; $c <= 12; $c ++ ) {
					?>
					<div class="small-1 columns">
						<div class="inside-column"></div>
					</div><?php
				}

			?></div>
		</div> <!-- #sa-framework-grid-lines --><?php

	}

}, 100 );

//! Add favicons To <head>
add_action( 'wp_head', 'sa_framework_add_favicons' );
add_action( 'admin_head', 'sa_framework_add_favicons' );
add_action( 'login_head', 'sa_framework_add_favicons' );
function sa_framework_add_favicons() {
	global $sa_framework_dir;

	// Set the images folder
	$images_folder = $sa_framework_dir . 'images/favicons/';

	// Include the favicons
	?><link rel="shortcut icon" href="<?php echo $images_folder; ?>ua-favicon-60.png" />
	<link rel="apple-touch-icon" href="<?php echo $images_folder; ?>ua-favicon-60.png" /> <?php /* Defaults to 60 */ ?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $images_folder; ?>ua-favicon-57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $images_folder; ?>ua-favicon-57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $images_folder; ?>ua-favicon-72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $images_folder; ?>ua-favicon-76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $images_folder; ?>ua-favicon-114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $images_folder; ?>ua-favicon-120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $images_folder; ?>ua-favicon-144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $images_folder; ?>ua-favicon-152.png" /><?php

}

//! Hide admin bar if user can't access the admin/dashboard
add_action( 'after_setup_theme', function() {

	// If the user cannot "read", then they cannot access the admin/dashboard
	if ( ! current_user_can( 'read' ) ) {
		show_admin_bar( false );
	}

});

//! Hide Query Monitor if admin bar isn't showing
add_filter( 'qm/process', function( $show_qm, $is_admin_bar_showing ) {
	return $is_admin_bar_showing;
}, 10, 2 );

//! Setup login header URL
add_filter( 'login_headerurl', function( $login_header_url ) {
	return 'https://sa.ua.edu/';
});
	
//! Setup login styles
add_action( 'login_footer', function() {
	global $sa_framework_dir;

	?><style type="text/css">
		body {
			background: #770000;
		}
		#login h1 a {
			display: block;
			background: url( "<?php echo $sa_framework_dir; ?>images/logos/CapstoneA-SA-white.svg" ) center bottom no-repeat;
			background-size: 300px auto;
			width: 100%;
			height: 110px;
		}
		.login #backtoblog a,
		.login #nav a {
			color: #fff;
			opacity: 0.8;
		}
		.login #backtoblog a:hover,
		.login #nav a:hover {
			color: #fff;
			opacity: 1;
			text-decoration: underline;
		}
		#loginform {
			background: #fff;
			padding: 25px;
		}
		.login form {
			padding-bottom: 35px;
		}
		.login form .forgetmenot {
			float: none;
		}
		#login form p.submit {
			display: block;
			clear: both;
			margin: 20px 0 0 0;
		}
		.login form .button {
			display: block;
			background: #990000;
			float: none;
			width: 100%;
			height: auto !important;
			border: 0;
			color: #fff;
			cursor: pointer;
			padding: 12px 0 12px 0 !important;
			font-size: 1.1em;
			line-height: 1em !important;
			text-transform: uppercase;
		}
		.login form .button:hover {
			background: #770000;
		}
	</style><?php
		
}, 100 );

//! Returns site's domain
function get_sa_domain() {

	// Make sure we have the HTTP HOST
	if ( ! ( $http_host = isset( $_SERVER[ 'HTTP_HOST' ] ) ? $_SERVER[ 'HTTP_HOST' ] : NULL ) ) {
		return false;
	}

	return $http_host;

}

//! Returns site's subdomain slug
function get_sa_subdomain_slug() {

	// Get the domain
	if ( ! ( $domain = get_sa_domain() ) ) {
		return false;
	}

	// Make sure it matches the sa.ua.edu domain
	if ( preg_match( '/([a-z0-9\-]+)\.(sa\.)?ua\.edu/i', $domain, $domain_matches )
	     && isset( $domain_matches[ 1 ] )
	     && ( $domain_match = $domain_matches[ 1 ] ) ) {

		return $domain_match;

	}

	return false;

}

//! Start the off-canvas menu
add_action( 'sa_framework_start_off_canvas_main_menu', 'sa_framework_start_off_canvas_main_menu' );
function sa_framework_start_off_canvas_main_menu() {
	global $sa_framework_dir;

	// Should we display the off canvas for the main menu per settings? - true by default
	$display_off_canvas_main_menu = wp_cache_get( 'display_off_canvas_main_menu', 'sa_framework' );

	// Display off canvas main menu
	if ( $display_off_canvas_main_menu ) {

		// Build off canvas menu args
		$menu_args = array(
			'theme_location' => 'sa-main',
			'container'      => false,
			'menu_class'     => false, //'menu main-menu off-canvas',
			'menu_id'        => 'sa-off-canvas-main-menu',
			'echo'           => false,
			'items_wrap'     => '<ul id="%1$s">%3$s</ul>',
			'depth'          => 0,
		);

		// Filter the off canvas menu args
		$menu_args = apply_filters( 'sa_framework_main_menu_args', $menu_args );

		// Get menu - make sure echo is set to false
		if ( $off_canvas_main_menu = wp_nav_menu( array_merge( $menu_args, array( 'echo' => false ) ) ) ) {

			// Define the search field title/placeholder text
			$search_placeholder_text = apply_filters( 'sa_framework_off_canvas_search_placeholder_text', 'Search Student Affairs' );

			// Make sure we have text
			if ( ! $search_placeholder_text ) {
				$search_placeholder_text = 'Search Student Affairs';
			}

			do_action( 'sa_framework_before_off_canvas_main_menu' );

			// Echo the off canvas menu
			?><div id="sa-off-canvas-main-menu-wrapper">
				<nav class="sa-off-canvas-menu-nav">
					<div class="sa-off-canvas-menu-header">
						<a href="https://sa.ua.edu/">
							<img src="<?php echo $sa_framework_dir; ?>images/logos/CapstoneA-SA.svg" alt="The University of Alabama Division of Student Affairs" />
						</a>
					</div>
					<div id="sa-off-canvas-search">
						<form role="search" method="get" class="search-form" action="https://sa.ua.edu/search/">
							<input type="hidden" name="cx" value="014059076202054157577:ozsjpjxzrhi">
							<input type="hidden" name="cof" value="FORID:10">
							<input type="hidden" name="ie" value="UTF-8">
							<label for="sa-off-canvas-search-field">
								<span class="screen-reader-text"><?php echo $search_placeholder_text; ?></span>
								<input id="sa-off-canvas-search-field" type="search" class="search-field" placeholder="<?php echo esc_attr( $search_placeholder_text  . '&hellip;' ); ?>" value="" name="s" title="<?php echo esc_attr( $search_placeholder_text ); ?>" />
							</label>
							<input id="sa-off-canvas-search-submit" type="submit" class="search-submit" value="<?php echo esc_attr( 'Search' ); ?>" />
							<div class="search-icon"></div> <!-- .search-icon -->
						</form>
					</div> <!-- #sa-off-canvas-search --><?php
					echo $off_canvas_main_menu;
				?></nav> <!-- .sa-off-canvas-menu-nav -->
			</div><?php

			do_action( 'sa_framework_after_off_canvas_main_menu' );

		}

	}

}

//! Print the banner
add_action( 'sa_framework_print_banner', 'sa_framework_print_banner' );
function sa_framework_print_banner() {

	// Include the banner template
	get_template_part( 'banner' );

}

// If no child header is added, print a basic header
add_action( 'init', function() {

	if ( ! has_action( 'sa_framework_print_header' ) ) {
		add_action( 'sa_framework_print_header', 'sa_framework_print_basic_header' );
	}

});

// If no child header is added, this will print a basic header
function sa_framework_print_basic_header() {
	global $sa_framework_site_slug;

	// Setup header wrapper classes - has UA square by default
	$header_wrapper_classes = array( "sa-{$sa_framework_site_slug}" );

	?><div id="sa-header-wrapper" class="<?php echo implode( ' ', $header_wrapper_classes ); ?>">
		<div class="row">
			<div class="small-12 columns">

				<div id="sa-header"><?php

					do_action( 'sa_framework_before_site_title' );

					?><div class="sa-site-title-wrapper">
						<a href="<?php echo get_bloginfo( 'url' ); ?>">
							<h1 class="sa-site-title"><?php echo apply_filters( 'sa_framework_header_site_title', get_bloginfo( 'title' ) ); ?></h1>
						</a>
					</div><?php

					do_action( 'sa_framework_after_site_title' );

				?></div> <!-- #sa-header -->

			</div>
		</div>
	</div> <!-- #sa-header-wrapper --><?php
}

//! Print the main menu
add_action( 'sa_framework_print_main_menu', 'sa_framework_print_main_menu' );
function sa_framework_print_main_menu() {

	// Include the main menu template
	get_template_part( 'main-menu' );

}

//! Start main area with columns
add_action( 'sa_framework_start_main_area', 'sa_framework_start_main_area' );
function sa_framework_start_main_area() {
	global $post;

	// Get this page's defined layout
	$defined_page_layout =  wp_cache_get( 'page_layout', 'sa_framework' );

	// Define main classes
	$sa_main_classes = array();

	// Define columns
	$columns = array();

	// Build columns
	switch( strtolower( $defined_page_layout ) ) {

		case 'left sidebar':
		case 'left-sidebar':
		case 'right sidebar':
		case 'right-sidebar':

			// Let's the main area know we have a sidebar
			$sa_main_classes[] = 'has-sidebar';

			// Take care of left sidebar
			if ( in_array( strtolower( $defined_page_layout ), array( 'left sidebar', 'left-sidebar' ) ) ) {

				$columns = array(
					'left-sidebar'  => array(
						'selectors' => array( 'sa-sidebar', 'sa-left-sidebar' ),
						'columns'   => array( 'small-12', 'medium-3', 'large-3' ),
					),
					'content'       => array(
						'ID'        => 'sa-content',
						'selectors' => array( 'sa-content' ),
						'columns'   => array( 'small-12', 'medium-9', 'large-9' ),
					),
				);

				// Let's the main area know we have a sidebar
				$sa_main_classes[] = 'has-left-sidebar';

			}

			// Take care of right sidebar
			else if ( in_array( strtolower( $defined_page_layout ), array( 'right sidebar', 'right-sidebar' ) ) ) {

				$columns = array(
					'content'       => array(
						'ID'        => 'sa-content',
						'selectors' => array( 'sa-content' ),
						'columns'   => array( 'small-12', 'medium-9', 'large-9' ),
					),
					'right-sidebar'  => array(
						'selectors' => array( 'sa-sidebar', 'sa-right-sidebar' ),
						'columns'   => array( 'small-12', 'medium-3', 'large-3' ),
					),
				);

				// Let's the main area know we have a sidebar
				$sa_main_classes[] = 'has-right-sidebar';

			}

			break;

		case '1 full width column':
		case 'full-width-column':
		default:
			$columns = array(
				'content'       => array(
					'ID'        => 'sa-content',
					'selectors' => array( 'sa-content' ),
					'columns'   => array( 'small-12' ),
				),
			);
			break;

	}

	do_action( 'sa_framework_before_main' );

	?><div id="sa-main"<?php echo ! empty( $sa_main_classes ) ? ( ' class="' . implode( ' ', $sa_main_classes ) . '"' ) : null; ?>>

		<div class="row"><?php

			foreach( $columns as $column_id => $column ) {

				// Filter column css selectors and columns
				$column_css_selectors = apply_filters( 'sa_framework_main_column_css_selectors', $column[ 'selectors' ], $column_id );
				$column_css_columns = apply_filters( 'sa_framework_main_column_css_columns', $column[ 'columns' ], $column_id );

				?><div<?php echo ! empty( $column[ 'ID' ] ) ? ' ID="' . $column[ 'ID' ] . '"' : null; ?> class="<?php echo implode( ' ', $column_css_selectors ) . ' ' . implode( ' ', $column_css_columns ); ?> columns"><?php

					switch( $column_id ) {

						case 'left-sidebar':

							do_action( 'sa_framework_before_sidebar' );

							do_action( 'sa_framework_before_left_sidebar' );

							do_action( 'sa_framework_print_sidebar' );

							do_action( 'sa_framework_print_left_sidebar' );

							do_action( 'sa_framework_after_sidebar' );

							do_action( 'sa_framework_after_left_sidebar' );

							break;

						case 'right-sidebar':

							do_action( 'sa_framework_before_sidebar' );

							do_action( 'sa_framework_before_right_sidebar' );

							do_action( 'sa_framework_print_sidebar' );

							do_action( 'sa_framework_print_right_sidebar' );

							do_action( 'sa_framework_after_sidebar' );

							do_action( 'sa_framework_after_right_sidebar' );

							break;


						case 'content':

							do_action( 'sa_framework_print_content_area' );

							break;

					}

				?></div><?php

			}

		?></div> <!-- .row --><?php

}

//! Close main area
add_action( 'sa_framework_close_main_area', 'sa_framework_close_main_area' );
function sa_framework_close_main_area() {
	?></div> <!-- #sa-main --><?php

	do_action( 'sa_framework_after_main' );

}

//! Print the banner breadcrumbs
add_action( 'sa_framework_print_banner_breadcrumbs', 'sa_framework_print_banner_breadcrumbs' );
function sa_framework_print_banner_breadcrumbs() {

	// Filter the banner breadcrumbs setting - true by default
	$display_banner_breadcrumbs = ( false === apply_filters( 'sa_framework_display_banner_breadcrumbs', true ) ) ? false : true;

	// Display banner breadcrumbs
	if ( $display_banner_breadcrumbs && ( $banner_breadcrumbs = get_sa_banner_breadcrumbs() ) ) {

		do_action( 'sa_framework_before_banner_breadcrumbs' );

		echo $banner_breadcrumbs;

		do_action( 'sa_framework_after_banner_breadcrumbs' );

	}

}

//! Print the search result breadcrumbs
add_action( 'sa_framework_after_item_title', 'sa_framework_print_search_result_breadcrumbs', 10 );
function sa_framework_print_search_result_breadcrumbs() {

	// Only for search
	if ( ! IS_SA_SEARCH_PAGE ) {
		return false;
	}

	// Filter the search result breadcrumbs setting - true by default
	$display_search_result_breadcrumbs = ( false === apply_filters( 'sa_framework_display_search_result_breadcrumbs', true ) ) ? false : true;

	// Display search result breadcrumbs
	if ( $display_search_result_breadcrumbs && ( $search_result_breadcrumbs = get_sa_search_result_breadcrumbs() ) ) {

		do_action( 'sa_framework_before_search_result_breadcrumbs' );

		echo $search_result_breadcrumbs;

		do_action( 'sa_framework_after_search_result_breadcrumbs' );

	}

}

//! Print the page breadcrumbs
add_action( 'sa_framework_before_content_area', 'sa_framework_print_page_breadcrumbs' );
function sa_framework_print_page_breadcrumbs() {
	global $post;

	// Don't show on the home page
	if ( is_front_page() ) {
		return;
	}

	// Get the breadcrumbs settings - true by default
	$display_page_breadcrumbs = ( isset( $post ) && strcasecmp( 'no', get_post_meta( $post->ID, 'sa_framework_display_page_breadcrumbs', true ) ) == 0 ) ? false : true;

	// Filter the breadcrumbs setting - true by default
	$display_page_breadcrumbs = ( false === apply_filters( 'sa_framework_display_page_breadcrumbs', $display_page_breadcrumbs ) ) ? false : true;

	// Display breadcrumbs
	if ( $display_page_breadcrumbs && ( $page_breadcrumbs = get_sa_page_breadcrumbs() ) ) {

		do_action( 'sa_framework_before_page_breadcrumbs' );

		echo $page_breadcrumbs;

		do_action( 'sa_framework_after_page_breadcrumbs' );

	}

}

//! Print the page pagination
add_action( 'sa_framework_print_page_pagination', 'sa_framework_print_page_pagination' );
function sa_framework_print_page_pagination() {

	// Display pagination
	if ( $page_pagination = get_sa_page_pagination() ) {

		do_action( 'sa_framework_before_page_pagination' );

		echo $page_pagination;

		do_action( 'sa_framework_after_page_pagination' );

	}

}

//! Print the page submenu at the top of the left sidebar
add_action( 'sa_framework_before_left_sidebar', 'sa_framework_print_page_submenu', 1 );
function sa_framework_print_page_submenu() {
	global $post;

	// Do we want to display the page submenu?
	$defined_display_page_submenu = null;

	// Get the display page submenu settings - default is null
	$defined_display_page_submenu = is_front_page() ? get_option( 'options_sa_framework_default_display_page_submenu' ) : ( isset( $post ) ? get_post_meta( $post->ID, 'sa_framework_display_page_submenu', true ) : null );

	// If no defined setting, get the set default layout - default is 'Yes'
	if ( ! $defined_display_page_submenu || strcasecmp( 'default', $defined_display_page_submenu ) == 0 ) {
		$defined_display_page_submenu = get_option( 'options_sa_framework_default_display_page_submenu' );
	}

	// Filter the setting
	$defined_display_page_submenu = apply_filters( 'sa_framework_display_page_submenu', $defined_display_page_submenu );

	// If set to no, get out of here
	if ( strcasecmp( 'no', $defined_display_page_submenu ) == 0 ) {
		return false;
	}

	do_action( 'sa_framework_before_page_submenu' );

	// Is 404?
	$is_404 = is_404();

	// Build menu args
	$menu_args = array(
		'container'         => false,
		'menu_class'        => 'menu submenu',
		'menu_id'           => 'sa-submenu',
		'echo'              => false,
		'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	);

	// See if we have a custom submenu
	$sa_framework_page_submenu = false;
	if ( isset( $post ) && strcasecmp( 'yes', get_post_meta( $post->ID, 'sa_framework_display_custom_page_submenu', true ) ) == 0 ) {

		// Get field
		$sa_framework_page_submenu = get_post_meta( $post->ID, 'sa_framework_page_submenu', true );

	}

	// Filter the page submenu
	$sa_framework_page_submenu = apply_filters( 'sa_framework_page_submenu', $sa_framework_page_submenu );

	// Get custom page submenu
	if ( $sa_framework_page_submenu ) {

		// Get menu from ID
		$menu_args[ 'menu' ]  = $sa_framework_page_submenu;
		$menu_args[ 'depth' ] = 0;

	}

	// Get default main menu
	else {

		// Get menu from location
		if ( IS_SA_SEARCH_PAGE ) {
			$menu_args[ 'theme_location' ] = 'sa-search-results';
		} else if ( $is_404 ) {
			$menu_args[ 'theme_location' ] = 'sa-404';
		} else {
			$menu_args[ 'theme_location' ] = 'sa-main';
		}

		// Set other arguments
		$menu_args[ 'second_level_menu' ] = ! ( IS_SA_SEARCH_PAGE || $is_404 );
		$menu_args[ 'depth' ] = IS_SA_SEARCH_PAGE || $is_404 ? 1 : 0;

	}

	// Filter the submenu args
	$menu_args = apply_filters( 'sa_framework_page_submenu_args', $menu_args );

	// If there's no menu with our location, get out of here
	if ( $menu_args[ 'theme_location' ] && ( $locations = get_nav_menu_locations() ) && ! isset( $locations[ $menu_args[ 'theme_location' ] ] ) ) {
		return false;
	}

	// Get/echo the menu
	if ( $the_submenu = wp_nav_menu( $menu_args ) ) {

		// Get the page section ID
		$page_section_id = wp_cache_get( 'menu_section_post_id', 'sa_framework' );

		// Get the page section title
		$page_section_title = get_the_title( $page_section_id ? $page_section_id : null );

		// Filter the section header to become the submenu header
		$submenu_header = apply_filters( 'sa_framework_submenu_header', $page_section_title, $menu_args );

		// Filter whether the submenu header should be displayed - false by default
		$display_submenu_header = apply_filters( 'sa_framework_display_submenu_header', false, $submenu_header, $menu_args );

		// Display the submenu header
		if ( $display_submenu_header && $submenu_header ) {

			// Get the submenu header permalink
			$submenu_header_permalink = $page_section_id ? get_permalink( $page_section_id ) : false;

			// Filter the permalink
			$submenu_header_permalink = apply_filters( 'sa_framework_submenu_header_permalink', $submenu_header_permalink, $menu_args );

			?><h3 class="sidebar-header"><?php

				if ( $submenu_header_permalink ) {
					?><a href="<?php echo $submenu_header_permalink; ?>"><?php echo $submenu_header; ?></a><?php
				} else {
					echo $submenu_header;
				}

			?></h3><?php

		}

		// Print the submenu
		echo $the_submenu;

	}

	do_action( 'sa_framework_after_page_submenu' );

}

//! Print the main content area
add_action( 'sa_framework_print_content_area', 'sa_framework_print_content_area' );
function sa_framework_print_content_area() {

	// Is it the front page?
	$is_front_page = is_front_page();

	// Is archive?
	$is_archive = is_archive();

	// Is 404?
	$is_404 = is_404();

	do_action( 'sa_framework_before_content_area' );

	if ( $is_front_page ) {
		do_action( 'sa_framework_before_front_page_content_area' );
	}

	if ( $is_archive || IS_SA_SEARCH_PAGE || $is_404 ) {

		// Do we want to display the page title?
		if ( apply_filters( 'sa_framework_display_page_title', true ) ) {

			do_action( 'sa_framework_before_page_title' );

			// Get page title
			$page_title = null;

			if ( IS_SA_SEARCH_PAGE ) {
				$page_title = 'Search Results';
			} else if ( $is_404 ) {
				$page_title = 'Page Not Found';
			} else if ( is_post_type_archive() ) {
				$page_title = post_type_archive_title( '', false );
			} else if ( is_day() ) {
				$page_title = 'Daily Archives: ' . get_the_date();
			} else if ( is_month() ) {
				$page_title = get_the_date( 'F Y' );
			} else if ( is_year() ) {
				$page_title = get_the_date( 'Y' );
			} else if ( $term = single_term_title( null, false ) ) {
				$page_title = "Archives: '$term'";
				if ( is_category() ) {
					$page_title = "Category $page_title";
				}
			} else {
				$page_title = 'Archives';
			}

			// Filter the page title
			$page_title = apply_filters( 'sa_framework_archive_title', $page_title );

			if ( IS_SA_SEARCH_PAGE ) {
				$page_title = apply_filters( 'sa_framework_search_results_title', $page_title );
			}

			// Display page title
			if ( $page_title ) {
				?><h1 class="page-title"><?php echo $page_title ?></h1><?php
			}

			do_action( 'sa_framework_after_page_title' );

		}

	}

	do_action( 'sa_framework_print_main_loop' );

	do_action( 'sa_framework_after_content_area' );

	if ( $is_front_page ) {
		do_action( 'sa_framework_after_front_page_content_area' );
	}

}

//! Print the main WordPress loop
add_action( 'sa_framework_print_main_loop', 'sa_framework_print_main_loop' );
function sa_framework_print_main_loop() {

	// Is it the front page?
	$is_front_page = is_front_page();

	// Is archive?
	$is_archive = is_archive();

	do_action( 'sa_framework_before_main_loop' );

	if ( $is_front_page ) {
		do_action( 'sa_framework_before_front_page_main_loop' );
	}

	// If there are no posts
	if ( ! have_posts() ) {

		if ( IS_SA_SEARCH_PAGE ) {
			do_action( 'sa_framework_no_search_results' );
		} else if ( $is_archive ) {
			do_action( 'sa_framework_no_posts' );
		} else {
			do_action( 'sa_framework_404' );
		}

		// Show the posts
	} else {

		do_action( 'sa_framework_print_page_pagination' );

		if ( $is_archive || IS_SA_SEARCH_PAGE ) {
			?><div class="sa-content-archive-posts<?php echo IS_SA_SEARCH_PAGE ? ' sa-content-search-results' : null; ?>"><?php
		}

		while ( have_posts() ) :
			the_post();

			// Get the ID
			$this_post_id = get_the_ID();

			if ( $is_archive || IS_SA_SEARCH_PAGE ) {

				?><div id="post-<?php echo $this_post_id; ?>" <?php post_class( 'sa-content-archive-post' . ( IS_SA_SEARCH_PAGE ? ' sa-content-search-result' : null ) ); ?>><?php

				do_action( 'sa_framework_before_item_title' );

				?><h2 class="item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2><?php

				do_action( 'sa_framework_after_item_title' );

				the_excerpt();

				?></div><?php

			} else {

				?><div id="post-<?php echo $this_post_id; ?>" <?php post_class( 'sa-content-post' ); ?>><?php

					// If not front page and we want to display the page title
					if ( ! $is_front_page && apply_filters( 'sa_framework_display_page_title', true ) ) {

						do_action( 'sa_framework_before_page_title' );

						// Get page title
						$page_title = apply_filters( 'sa_framework_page_title', get_the_title(), $this_post_id );

						// Display page title
						if ( $page_title ) {
							?><h1 class="page-title"><?php echo $page_title; ?></h1><?php
						}

						do_action( 'sa_framework_after_page_title' );

					}

					do_action( 'sa_framework_before_content' );

					the_content();

					do_action( 'sa_framework_after_content' );

				?></div><?php

			}

		endwhile;

		if ( $is_archive || IS_SA_SEARCH_PAGE ) {
			?></div><?php
		}

	}

	do_action( 'sa_framework_after_main_loop' );

	if ( $is_front_page ) {
		do_action( 'sa_framework_after_front_page_main_loop' );
	}

}

//! Print 404 message
add_action( 'sa_framework_404', 'sa_framework_print_404_message' );
function sa_framework_print_404_message() {

	?><p>We're sorry, but the page you have requested cannot be found on this website. Please check to make sure the link you requested was entered correctly.</p>

	<p><strong>Here are some suggestions:</strong></p>
	<ul>
		<li>Browse the site's menu.</li>
		<li>Use the site's search feature.</li>
	</ul>

	<p>Please help us keep this site current and accurate by <a href="<?php echo get_bloginfo('url'); ?>/contact/">reporting broken links</a>.</p><?php

}

//! Print "no search results" message
add_action( 'sa_framework_no_search_results', 'sa_framework_print_no_search_results_message' );
function sa_framework_print_no_search_results_message() {

	?><p>We're sorry, but it appears there is no content that matches <strong><?php echo get_search_query(); ?></strong>.</p>

	<p><strong>Here are some suggestions:</strong></p>
	<ul>
		<li>Make sure all words are spelled correctly.</li>
		<li>Try using more general, or fewer, keywords.</li>
	</ul>

	<p>If you believe this is an error, or need further assistance, please <a href="<?php echo get_bloginfo('url'); ?>/contact/">let us know</a>.</p><?php

}

//! Get banner breadcrumbs - set $format to false if you want the array instead of the HTML
function get_sa_banner_breadcrumbs( $format = true ) {

	// Build breadcrumbs
	$breadcrumbs = array();

	// Add UA
	$breadcrumbs[ 'ua' ] = array(
		'url' => 'http://ua.edu/',
		'title' => 'The University of Alabama',
		'label' => '<span class="hide-for-large-up">UA</span><span class="show-for-large-up">The University of Alabama</span>',
	);

	// Add Student Affairs
	$breadcrumbs[ 'sa' ] = array(
		'url'   => 'https://sa.ua.edu/',
		'label' => 'Student Affairs',
	);

	// Build current site label
	$current_site_label = get_bloginfo( 'name' );

	// If we're set to show a different current site label for small screens...
	if ( $site_small_label = get_option( 'options_sa_framework_banner_breadcrumbs_site_small_label' ) ) {

		// First, wrap in visibility
		$current_site_label = '<span class="show-for-medium-up">' . $current_site_label . '</span>';

		// Then add slug for small screens
		$current_site_label = '<span class="show-for-small-only">' . $site_small_label . '</span>' . $current_site_label;

	}

	// Add current site
	$breadcrumbs[ 'current' ] = array(
		'label' => $current_site_label,
	);

	// Add current site URL if not home page
	if ( ! is_front_page() ) {
		$breadcrumbs[ 'current' ][ 'url' ] = get_bloginfo( 'url' );
	}

	// Filter the breadcrumbs array
	$breadcrumbs = apply_filters( 'sa_framework_banner_breadcrumbs', $breadcrumbs );

	// If no breadcrumbs or we don't want them formatted, get out of here
	if ( ! $format || ! $breadcrumbs )
		return $breadcrumbs;

	// Build breadcrumbs HTML
	$breadcrumbs_html = null;

	foreach( $breadcrumbs as $crumb_key => $crumb ) {

		// Make sure we have what we need
		if ( empty( $crumb[ 'label' ] ) ) {
			continue;
		}

		// Setup classes
		$crumb_classes = array( $crumb_key );

		// Add if current
		if ( isset( $crumb[ 'current' ] ) && $crumb[ 'current' ] ) {
			$crumb_classes[] = 'current';
		}

		$breadcrumbs_html .= '<li role="menuitem"' . ( ! empty( $crumb_classes ) ? ' class="' . implode( ' ', $crumb_classes ) . '"' : null ) . '>';

		// Add URL and label
		if ( ! empty( $crumb[ 'url' ] ) ) {
			$breadcrumbs_html .= '<a href="' . $crumb[ 'url' ] . '"' . ( ! empty( $crumb[ 'title' ] ) ? ' title="' . $crumb[ 'title' ] . '"' : null ) . '>' . $crumb[ 'label' ] . '</a>';
		} else {
			$breadcrumbs_html .= $crumb[ 'label' ];
		}

		$breadcrumbs_html .= '</li>';

	}

	// Wrap them in nav
	$breadcrumbs_html = '<ul id="sa-banner-breadcrumbs" role="menubar" aria-label="breadcrumbs">' . $breadcrumbs_html . '</ul>';

	return $breadcrumbs_html = apply_filters( 'sa_framework_banner_breadcrumbs_html', $breadcrumbs_html, $breadcrumbs );

}

//! Get search result breadcrumbs - set $format to false if you want the array instead of the HTML
function get_sa_search_result_breadcrumbs( $format = true ) {
	global $post;

	// Build breadcrumbs
	$breadcrumbs = array();

	// Add home
	$breadcrumbs[ 'home' ] = array(
		'url'   => get_bloginfo( 'url' ),
		'label' => 'Home',
	);

	// If not a "post" or "page", then try to get archive
	if ( ! in_array( $post->post_type, array( 'page', 'post' ) ) ) {

		// If archive exists, add archive
		if ( $post_type_archive_url = get_post_type_archive_link( $post->post_type ) ) {

			// Get the post type archive title
			$post_type_obj = get_post_type_object( $post->post_type );

			// Filter the post type archive title.
			$post_type_archive_title = apply_filters( 'post_type_archive_title', $post_type_obj->labels->name, $post->post_type );

			$breadcrumbs[] = array(
				'url'   => $post_type_archive_url,
				'label' => $post_type_archive_title,
			);

		}

		// If page with archive slug exists, add page
		else if ( ( $post_type_object = get_post_type_object( $post->post_type ) )
		    && isset( $post_type_object->rewrite )
		    && isset( $post_type_object->rewrite[ 'slug' ] )
		    && ( $post_type_page = get_page_by_path( $post_type_object->rewrite[ 'slug' ] ) ) ) {

			$breadcrumbs[] = array(
				'url'   => get_permalink( $post_type_page ),
				'label' => get_the_title( $post_type_page->ID )
			);

		}

	}

	// If we have ancestors, add them
	if ( $post_ancestors = get_post_ancestors( $post->ID ) ) {
		foreach( $post_ancestors as $post_ancestor_id ) {

			// Add ancestor
			$breadcrumbs[] = array(
				'url'   => get_permalink( $post_ancestor_id ),
				'label' => get_the_title( $post_ancestor_id ),
			);

		}
	}

	// Add current page - if not home page
	if ( ! is_front_page() ) {
		$breadcrumbs[ 'current' ] = array(
			'url'   => get_permalink( $post->ID ),
			'label' => get_the_title( $post->ID ),
		);
	}

	// Filter the breadcrumbs array
	$breadcrumbs = apply_filters( 'sa_framework_search_result_breadcrumbs', $breadcrumbs, $post );

	// If no breadcrumbs or we don't want them formatted, get out of here
	if ( ! $format || ! $breadcrumbs )
		return $breadcrumbs;

	// Build breadcrumbs HTML
	$breadcrumbs_html = null;

	foreach( $breadcrumbs as $crumb_key => $crumb ) {

		// Make sure we have what we need
		if ( empty( $crumb[ 'url' ] ) || empty( $crumb[ 'label' ] ) ) {
			continue;
		}

		// If no string crumb key, set as ancestor
		if ( ! $crumb_key || is_numeric( $crumb_key ) ) {
			$crumb_key = 'ancestor';
		}

		// Setup classes
		$crumb_classes = array( $crumb_key );

		// Add if current
		if ( isset( $crumb[ 'current' ] ) && $crumb[ 'current' ] ) {
			$crumb_classes[] = 'current';
		}

		$breadcrumbs_html .= '<li role="menuitem"' . ( ! empty( $crumb_classes ) ? ' class="' . implode( ' ', $crumb_classes ) . '"' : null ) . '>';

		// Add URL and label
		if ( ! empty( $crumb[ 'url' ] ) ) {
			$breadcrumbs_html .= '<a href="' . $crumb[ 'url' ] . '"' . ( ! empty( $crumb[ 'title' ] ) ? ' title="' . $crumb[ 'title' ] . '"' : null ) . '>' . $crumb[ 'label' ] . '</a>';
		} else {
			$breadcrumbs_html .= $crumb[ 'label' ];
		}

		$breadcrumbs_html .= '</li>';

	}

	// Wrap them in nav
	$breadcrumbs_html = '<ul class="breadcrumbs sa-search-result-breadcrumbs" role="menubar" aria-label="breadcrumbs">' . $breadcrumbs_html . '</ul>';

	return $breadcrumbs_html = apply_filters( 'sa_framework_search_result_breadcrumbs_html', $breadcrumbs_html, $breadcrumbs, $post );

}

//! Get page breadcrumbs - set $format to false if you want the array instead of the HTML
function get_sa_page_breadcrumbs( $format = true ) {
	global $post, $sa_breadcrumbs, $sa_page_breadcrumbs_html;

	// Return breadcrumbs if they're already set
	if ( ! $format && ! empty( $sa_breadcrumbs ) ) {
		return $sa_breadcrumbs;
	} else if ( $format && ! empty( $sa_page_breadcrumbs_html ) ) {
		return $sa_page_breadcrumbs_html;
	}

	// Get post type
	$post_type = get_query_var( 'post_type' );

	// Build array of breadcrumbs
	$breadcrumbs = array();

	// Add home
	$breadcrumbs[] = array(
		'url'   => get_bloginfo( 'url' ),
		'label' => 'Home',
	);

	// For 404 pages
	if ( is_404() ) {

		$breadcrumbs[ '404' ] = array(
			'current'   => true,
			'label'     => 'Page Not Found',
		);

	}

	// For search pages
	else if ( IS_SA_SEARCH_PAGE ) {

		$breadcrumbs[ 'search' ] = array(
			'current'   => true,
			'url'       => get_search_link(),
			'label'     => 'Search Results',
		);

	}

	// For post type archive pages
	else if ( is_post_type_archive() && $post_type ) {

		$breadcrumbs[ 'archive-post-type' ] = array(
			'current'   => true,
			'url'       => get_post_type_archive_link( $post_type ),
			'label'     => post_type_archive_title( '', false ),
		);

	}

	// For archive pages
	else if ( is_archive() ) {

		if ( is_category()
			&& ( $term_label = single_term_title( NULL, false ) )
			&& ( $category_url = get_category_link( get_cat_ID( $term_label ) ) ) ) {

			$breadcrumbs[ 'archive-category' ] = array(
				'current'   => true,
				'url'       => $category_url,
				'label'     => $term_label,
			);

		} else if ( is_day() ) {

			$breadcrumbs[ 'archive-day' ] = array(
				'current'   => true,
				'url'       => get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ),
				'label'     => get_the_date( 'F j, Y' ),
			);

		} else if ( is_month() ) {

			$breadcrumbs[ 'archive-month' ] = array(
				'current'   => true,
				'url'       => get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ),
				'label'     => get_the_date( 'F Y' ),
			);

		} else if ( is_year() ) {

			$breadcrumbs[ 'archive-year' ] = array(
				'current'   => true,
				'url'       => get_year_link( get_the_time( 'Y' ) ),
				'label'     => get_the_date( 'Y' ),
			);

		}

	}

	// Build crumbs around current page
	else if ( is_singular() && isset( $post ) ) {

		// If not a "page", then try to get archive
		if ( 'page' != $post->post_type ) {

			// If archive exists, add archive
			if ( $post_type_archive_url = get_post_type_archive_link( $post->post_type ) ) {

				// Get the post type archive title
				$post_type_obj = get_post_type_object( $post->post_type );

				// Filter the post type archive title.
				$post_type_archive_title = apply_filters( 'post_type_archive_title', $post_type_obj->labels->name, $post->post_type );

				//@TODO replace the above 4 lines with the following?
				//post_type_archive_title( '', false )

				$breadcrumbs[] = array(
					'url'   => $post_type_archive_url,
					'label' => $post_type_archive_title,
				);

			}

			// If page with archive slug exists, add page
			else if ( ( $post_type_object = get_post_type_object( $post->post_type ) )
			    && isset( $post_type_object->rewrite )
			    && isset( $post_type_object->rewrite[ 'slug' ] )
			    && ( $post_type_page = get_page_by_path( $post_type_object->rewrite[ 'slug' ] ) ) ) {

				$breadcrumbs[] = array(
					'ID'    => $post_type_page->ID,
					'url'   => get_permalink( $post_type_page ),
					'label' => get_the_title( $post_type_page->ID )
				);

			}

		}

		// If we have ancestors, add them
		if ( $post_ancestors = get_post_ancestors( $post->ID ) ) {

			// Store ancestors separately so we can reverse them
			$post_ancestors_crumbs = array();

			foreach( $post_ancestors as $post_ancestor_id ) {

				// Add ancestor
				$post_ancestors_crumbs[] = array(
					'ID'	=> $post_ancestor_id,
					'url'   => get_permalink( $post_ancestor_id ),
					'label' => get_the_title( $post_ancestor_id ),
				);

			}

			// Add to crumbs in reverse order
			$breadcrumbs = array_merge( $breadcrumbs, array_reverse( $post_ancestors_crumbs ) );

		}

		// Add current page - if not home page
		if ( ! is_front_page() ) {

			$breadcrumbs[ 'current-page' ] = array(
				'current'   => true,
				'ID'      => $post->ID,
				'url'     => get_permalink( $post ),
				'label'   => get_the_title( $post->ID ),
			);

		}

	}

	// Filter the breadcrumbs array - we change up the variable so it doesn't interfere with global variable
	$sa_breadcrumbs = apply_filters( 'sa_framework_page_breadcrumbs', $breadcrumbs, $post );

	// If no breadcrumbs or we don't want them formatted, get out of here
	if ( ! $format || ! $sa_breadcrumbs ) {
		return $sa_breadcrumbs;
	}

	// Build breadcrumbs HTML
	$breadcrumbs_html = null;

	foreach( $sa_breadcrumbs as $crumb_key => $crumb ) {

		// Make sure we have what we need
		if ( empty( $crumb[ 'label' ] ) ) {
			continue;
		}

		// Setup classes
		$crumb_classes = array();

		// If no string crumb key, set as ancestor
		if ( ! $crumb_key || is_numeric( $crumb_key ) ) {
			$crumb_classes[] = 'ancestor';
		}

		// Add if current
		if ( isset( $crumb[ 'current' ] ) && $crumb[ 'current' ] ) {
			$crumb_classes[] = 'current';
		}

		$breadcrumbs_html .= '<li role="menuitem"' . ( ! empty( $crumb_classes ) ? ' class="' . implode( ' ', $crumb_classes ) . '"' : null ) . '>';

		// Add URL and label
		if ( ! empty( $crumb[ 'url' ] ) ) {
			$breadcrumbs_html .= '<a href="' . $crumb[ 'url' ] . '"' . ( ! empty( $crumb[ 'title' ] ) ? ' title="' . $crumb[ 'title' ] . '"' : null ) . '>' . $crumb[ 'label' ] . '</a>';
		} else {
			$breadcrumbs_html .= $crumb[ 'label' ];
		}

		$breadcrumbs_html .= '</li>';

	}

	// Wrap them in nav
	$breadcrumbs_html = '<ul id="sa-page-breadcrumbs" class="breadcrumbs" role="menubar" aria-label="breadcrumbs">' . $breadcrumbs_html . '</ul>';

	//  We change up the variable so it doesn't interfere with global variable
	return $sa_page_breadcrumbs_html = apply_filters( 'sa_framework_page_breadcrumbs_html', $breadcrumbs_html, $sa_breadcrumbs, $post );

}

//! Get the page pagination
function get_sa_page_pagination( $args = array() ) {
	global $wp_query, $sa_pagination, $sa_page_pagination_html;

	// Setup arguments
	$defaults = array(
		'format'    => true, // Set to false if you want the array instead of the HTML
		'singular'  => IS_SA_SEARCH_PAGE ? 'result' : 'item',
		'plural'    => IS_SA_SEARCH_PAGE ? 'results' : 'items',
		'no_results'=> 'There are no ' . ( IS_SA_SEARCH_PAGE ? 'results' : 'items' ) . '.',
	);
	$args = wp_parse_args( $args, $defaults );

	// Return pagination if they're already set
	if ( ! $args[ 'format' ] && ! empty( $sa_pagination ) ) {
		return $sa_pagination;
	} else if ( $args[ 'format' ] && ! empty( $sa_page_pagination_html ) ) {
		return $sa_page_pagination_html;
	}

	// There's no point in including pagination if only one page
	$max_num_pages = isset( $wp_query->max_num_pages ) && $wp_query->max_num_pages > 0 ? $wp_query->max_num_pages : 0;
	if ( $max_num_pages <= 1 ) {
		return false;
	}

	// Store the pagination data
	$pg = (object) array(
		'post_count' => isset( $wp_query->post_count ) && $wp_query->post_count > 0 ? $wp_query->post_count : 0,
		'found_posts' => isset( $wp_query->found_posts ) && $wp_query->found_posts > 0 ? $wp_query->found_posts : 0,
		'paged' => ( $paged_var = get_query_var( 'paged' ) ) && $paged_var > 1 ? $paged_var : 1,
		'max_num_pages' => $max_num_pages,
		'posts_per_page' => get_query_var( 'posts_per_page' ),
	);

	// Setup "showing" parameters
	$pg->showing_start = $pg->paged > 1 ? ( $pg->posts_per_page * ( $pg->paged - 1 ) ) + 1 : 1;
	$pg->showing_end = ( $pg->showing_start - 1 ) + $pg->posts_per_page;

	// Can't be more than found posts
	if ( $pg->showing_end > $pg->found_posts ) {
		$pg->showing_end = $pg->found_posts;
	}

	// Set "showing" text
	$pg->showing_text = null;

	// If there are no posts...
	if ( ! $pg->post_count ) {
		$pg->showing_text = $args[ 'no_results' ];
	} else {

		// If there's only 1 item
		if ( 1 == $pg->found_posts ) {

			$pg->showing_text .= "Showing {$pg->found_posts} {$args['singular']}.";

		// If there's only one page
		} else if ( $pg->max_num_pages == 1 ) {

			$pg->showing_text .= "Showing {$pg->found_posts} {$args['plural']}.";

		// Everything else
		} else {

			$pg->showing_text .= "Showing {$pg->showing_start} - {$pg->showing_end} of {$pg->found_posts} {$args['plural']}.";

		}

	}

	// At which page count will we add ellipses
	$pg->ellipses_page_min = 6;

	// What's the starting index? - Never start at 1 because its always listed
	$pg->page_start_index = $pg->paged > 2 ? ( $pg->paged - 1 ) : 2;

	// How many non-first and non-last items to show
	$pg->pages_to_show = 3;

	// Unless we're at the beginning or end
	if ( $pg->paged <= 3 ) {

		// Reset index
		$pg->page_start_index = 2;

		// Only show 2 items
		$pg->pages_to_show = 2;

	} else if ( ( $pg->max_num_pages - $pg->paged ) <= 2 ) {

		// Reset index
		$pg->page_start_index = $pg->max_num_pages - 2;

		// Only show 2 items
		$pg->pages_to_show = 2;

	}

	// Make sure we never start at 1
	if ( $pg->page_start_index < 2 )
		$pg->page_start_index = 2;

	// Are we gonna show the first ellipses?
	$pg->show_first_ellipses = ( $pg->max_num_pages >= $pg->ellipses_page_min && $pg->page_start_index >= 3 );

	// Are we gonna show the last ellipses?
	$pg->show_last_ellipses = ( $pg->max_num_pages >= $pg->ellipses_page_min && $pg->paged < ( $pg->max_num_pages - 2 ) );

	// Filter the pagination array - we change up the variable so it doesn't interfere with global variable
	$sa_pagination = apply_filters( 'sa_framework_page_pagination', $pg, $wp_query );

	// If no pagination or we don't want them formatted, get out of here
	if ( ! $args[ 'format' ] || ! $sa_pagination )
		return $sa_pagination;

	// Build pagination HTML
	$pagination_html = null;

	// Build ellipses string to easily output
	$ellipses = '<li class="unavailable ellipses" aria-disabled="true">&hellip;</li>';

	// Add "showing" text"
	if ( $sa_pagination->showing_text ) {
		$pagination_html .= '<span class="showing">' . $sa_pagination->showing_text . '</span>';
	}

	// Start pagination list
	$pagination_html .= '<ul class="pagination" role="menubar" aria-label="Pagination">';

	// Add left arrow if more than one page
	if ( $sa_pagination->max_num_pages > 1 ) {

		// Create left arrow classes
		$left_arrow_classes = array( 'arrow', 'left-arrow' );

		// Is it unavailable?
		$left_arrow_unavailable = ( $sa_pagination->paged < 2 );

		if ( $left_arrow_unavailable ) {
			$left_arrow_classes[] = 'unavailable';
		}

		// Build left arrow string to easily output
		$pagination_html .= '<li class="' . implode( ' ', $left_arrow_classes ) . '"';

			if ( $left_arrow_unavailable ) {
				$pagination_html .= ' aria-disabled="true"';
			}

		$pagination_html .= '>';

		if ( $left_arrow_unavailable ) {
			$pagination_html .= '&laquo;';
		} else {
			$pagination_html .= '<a href="' . get_pagenum_link( $sa_pagination->paged - 1 ) . '" title="Previous page">&laquo;</a>';
		}

		$pagination_html .= '</li>';

	}

	// Print first item
	$pagination_html .= '<li' . ( 1 == $sa_pagination->paged ? ' class="current"' : null ) . '><a href="' . get_pagenum_link( 1 ) . '">1</a></li>';

	// Are we showing the first ellipses?
	if ( $sa_pagination->show_first_ellipses ) {
		$pagination_html .= $ellipses;
	}

	// Don't print the last one, we'll add it later
	for ( $p = $sa_pagination->page_start_index; $p < $sa_pagination->max_num_pages; $p++ ) {

		// Is this the current page?
		$is_current_page = isset( $sa_pagination->paged ) && $sa_pagination->paged == $p;

		// Create item classes
		$p_item_classes = array();

		// If current, add class
		if ( $is_current_page )
			$p_item_classes[] = 'current';

		// Build item link
		$p_item_link = get_pagenum_link( $p );

		// Create list item string
		$p_item_string = '<li' . ( ! empty( $p_item_classes ) ? ' class="' . implode( ' ', $p_item_classes ) . '"' : NULL ) . '><a href="' . $p_item_link . '">' . $p . '</a></li>';

		// If more than so many pages...
		if ( $sa_pagination->max_num_pages >= $sa_pagination->ellipses_page_min ) {

			// Only show so many
			if ( $p < ( $sa_pagination->page_start_index + $sa_pagination->pages_to_show ) ) {

				$pagination_html .= $p_item_string;

				// Quit loop after items
				if ( $p == ( ( $sa_pagination->page_start_index + $sa_pagination->pages_to_show ) - 1 ) )
					break;

			}

		// Otherwise, show everything
		} else {

			$pagination_html .= $p_item_string;

		}

	}

	// Are we showing the last ellipses?
	if ( $sa_pagination->show_last_ellipses ) {
		$pagination_html .= $ellipses;
	}

	// Print last item if more than one page
	if ( $sa_pagination->max_num_pages > 1 ) {
		$pagination_html .= '<li' . ( $sa_pagination->max_num_pages == $sa_pagination->paged ? ' class="current"' : null ) . '><a href="' . get_pagenum_link( $sa_pagination->max_num_pages ) . '">' . $sa_pagination->max_num_pages . '</a></li>';
	}

	// Add right arrow if more than one page
	if ( $sa_pagination->max_num_pages > 1 ) {

		// Create right arrow classes
		$right_arrow_classes = array( 'arrow', 'right-arrow' );

		// Is it unavailable?
		$right_arrow_unavailable = ( $sa_pagination->paged == $sa_pagination->max_num_pages );

		if ( $right_arrow_unavailable ) {
			$right_arrow_classes[] = 'unavailable';
		}

		// Build right arrow string to easily output
		$pagination_html .= '<li class="' . implode( ' ', $right_arrow_classes ) . '"';

			if ( $right_arrow_unavailable ) {
				$pagination_html .= ' aria-disabled="true"';
			}

		$pagination_html .= '>';

		if ( $right_arrow_unavailable ) {
			$pagination_html .= '&raquo;';
		} else {
			$pagination_html .= '<a href="' . get_pagenum_link( $sa_pagination->paged + 1 ) . '" title="Next page">&raquo;</a>';
		}

		$pagination_html .= '</li>';

	}

	// Close pagination list
	$pagination_html .= '</ul>';

	// Wrap them in nav
	$pagination_html = '<div class="sa-page-pagination">' . $pagination_html . '</div>';

	//  We change up the variable so it doesn't interfere with global variable
	return $sa_page_pagination_html = apply_filters( 'sa_framework_page_pagination_html', $pagination_html, $sa_pagination, $wp_query );

}

/**
 * This function allows you to retrieve the get_post_meta()
 * data for any post on your network. If you are running
 * a multisite network, you can supply another blog's ID to retrieve a post's
 * meta data from another site on your WordPress multisite network.
 *
 * @param int $post_id - the post ID
 * @param int $post_blog_id - blog ID. Defaults to current blog ID.
 * @param string $key     Optional. The meta key to retrieve. By default, returns
 *                        data for all keys. Default empty.
 * @param bool   $single  Optional. Whether to return a single value. Default false.
 * @return mixed Will be an array if $single is false. Will be value of meta data
 *               field if $single is true.
 */
function wp_get_get_post_meta( $post_id, $post_blog_id = 0, $key = '', $single = false ) {
	global $blog_id, $wpdb;

	// If a $post_blog_id was not supplied or we're on the same site
	// or not running a multisite, then use the WP functions.
	if ( ! $post_blog_id || $blog_id == $post_blog_id || ! is_multisite() ) {

		// Use the WordPress function.
		return get_post_meta( $post_id, $key, $single );

	} else {

		// Change $wpdb blog ID for queries.
		// Will change back when we're done.
		$wpdb->set_blog_id( $post_blog_id );

		// We have to retrieve the meta value manually.
		$meta_value = $wpdb->get_var( "SELECT meta_value FROM {$wpdb->postmeta} WHERE meta_key = '{$key}' AND post_id = {$post_id}" );

		// Reset the $wpdb blog ID.
		$wpdb->set_blog_id( $blog_id );

		return $meta_value;

	}

	return false;
}

/**
 * This function allows you to retrieve the wp_get_attachment_image_src()
 * data for any post's featured image on your network. If you are running
 * a multisite network, you can supply another blog's ID to retrieve a post's
 * featured image data from another site on your WordPress multisite network.
 *
 * Does not take care of icon business (at this time).
 *
 * If successful, this function returns an array of the following:
 *  [0] => url
 *  [1] => width
 *  [2] => height
 *  [3] => boolean: true if a resized image, false if it is the original
 *
 * Returns false if no image is available.
 *
 * Modified from https://gist.github.com/bamadesigner/f5a338805d28f9c15df7
 *
 * @param int $attachment_id - the attachment ID
 * @param int $attachment_blog_id - blog ID. Defaults to current blog ID.
 * @param string $size - Optional. Only takes size names, e.g. 'thumbnail', 'large', etc.,
 *	and not size arrays, e.g. array( 32, 32 ).
 * @return bool|array - Returns an array (url, width, height, resized image), or false, if no image is available.
 */
function wp_multisite_get_attachment_image_src( $attachment_id, $attachment_blog_id = 0, $size = NULL ) {
	global $blog_id, $wpdb;

	// If a $attachment_id was not supplied or we're on the same site
	// or not running a multisite, then use the WP functions.
	if ( ! $attachment_id || $blog_id == $attachment_blog_id || ! is_multisite() ) {

		// Use the WordPress function.
		return wp_get_attachment_image_src( $attachment_id, $size );

	} else {

		// Change $wpdb blog ID for queries.
		// Will change back when we're done.
		$wpdb->set_blog_id( $attachment_blog_id );

		// We have to retrieve the post thumbnail metadata manually.
		// Only runs if we found a $attachment_id.
		$post_thumbnail_metadata = $attachment_id ? maybe_unserialize( $wpdb->get_var( "SELECT meta_value FROM {$wpdb->postmeta} WHERE meta_key = '_wp_attachment_metadata' AND post_id = {$attachment_id}" ) ) : NULL;

		// Reset the $wpdb blog ID.
		$wpdb->set_blog_id( $blog_id );

		// No point in continuing if we didn't find any data
		if ( ! $post_thumbnail_metadata ) {
			return false;
		}

		// We need to figure out base files URL.
		// This is not the best method but I can't find a more
		// definite way of securing the upload directory for another
		// site on the network and wp_upload_dir() is too complicated
		// to re-create and tweak so we'll assume the upload directory
		// structure is the same for all sites and tweak the current
		// site's upload directory.
		$post_thumbnail_base_url = NULL;
		if ( ( $current_site_upload_dir = wp_upload_dir() )
		     && isset( $current_site_upload_dir[ 'baseurl' ] ) ) {

			// Create preg patterns for current site to replace with other site.
			$current_site_preg = preg_replace( "/([^a-z])/i", '\\\$1', get_site_url() );

			// Replace current site URL with other site URL.
			$post_thumbnail_base_url = preg_replace( '/^' . $current_site_preg . '/i', get_site_url( $attachment_blog_id ), $current_site_upload_dir[ 'baseurl' ] );

		}

		// No point going forward if we don't have the base URL or default file.
		if ( ! $post_thumbnail_base_url || ! isset( $post_thumbnail_metadata[ 'file' ] ) )
			return false;

		// Set default post thumbnail URL.
		$default_post_thumbnail_url = $post_thumbnail_base_url . '/' . $post_thumbnail_metadata[ 'file' ];

		// Set default post thumbnail width and height.
		$default_post_thumbnail_width = isset( $post_thumbnail_metadata[ 'width' ] ) ? $post_thumbnail_metadata[ 'width' ] : NULL;
		$default_post_thumbnail_height = isset( $post_thumbnail_metadata[ 'height' ] ) ? $post_thumbnail_metadata[ 'height' ] : NULL;

		// If no set size, or they want 'full' size, or size doesn't exist, get default URL and info.
		if ( ( ! $size || ( $size && ( 'full' == $size || ( ! ( isset( $post_thumbnail_metadata[ 'sizes' ] ) && isset( $post_thumbnail_metadata[ 'sizes' ][ $size ] ) ) ) ) ) ) && isset( $post_thumbnail_metadata[ 'file' ] ) ) {

			// Return default image size data.
			return array(
				$default_post_thumbnail_url,
				$default_post_thumbnail_width,
				$default_post_thumbnail_height,
				false,
			);

		}

		// Get specific size URL and info.
		else if ( ( $post_thumbnail_size = $post_thumbnail_metadata[ 'sizes' ][ $size ] )
		          && isset( $post_thumbnail_size[ 'file' ] ) ) {

			// We need the basename of the default URL to create the specific size URL.
			$default_post_thumbnail_url_basename = wp_basename( $default_post_thumbnail_url );

			// Create URL for specific size by merging with default URL.
			$post_thumbnail_size_url = str_replace( $default_post_thumbnail_url_basename, wp_basename( $post_thumbnail_size[ 'file' ] ), $default_post_thumbnail_url );

			// Set post thumbnail width and height for specific size.
			$post_thumbnail_size_width = isset( $post_thumbnail_size[ 'width' ] ) ? $post_thumbnail_size[ 'width' ] : NULL;
			$post_thumbnail_size_height = isset( $post_thumbnail_size[ 'height' ] ) ? $post_thumbnail_size[ 'height' ] : NULL;

			// Return specific image size data.
			return array(
					$post_thumbnail_size_url,
					$post_thumbnail_size_width,
					$post_thumbnail_size_height,
					( $post_thumbnail_size_width != $default_post_thumbnail_width && $post_thumbnail_size_height != $default_post_thumbnail_height ),
			);

		}

	}

	return false;
}