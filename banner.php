<?php

// Setup banner wrapper classes
$banner_wrapper_classes = apply_filters( 'sa_framework_banner_wrapper_classes', array() );

?><div id="sa-banner-wrapper" class="<?php echo implode( ' ', $banner_wrapper_classes ); ?>">
	<div class="row">
		<div class="small-12 columns">

			<div id="sa-banner"><?php

				// Print banner breadcrumbs
				do_action( 'sa_framework_print_banner_breadcrumbs' );

				// Add directory toggle
				?><div class="sa-banner-right">

					<a id="sa-directory" class="toggle-icon" href="https://sa.ua.edu/directory/">
						<div class="bar one"></div>
						<div class="bar two"></div>
						<div class="bar three"></div>
						<span class="toggle-label">Directory</span>
					</a><?php

					// Add "help" button
					?><a id="sa-help" href="https://sa.ua.edu/contact/">Contact Student Affairs</a><?php

					// Add search button
					?><a id="sa-search-site" href="<?php echo get_bloginfo('url'); ?>/search/">Search Student Affairs</a>

				</div>

			</div> <!-- #sa-banner -->

		</div>
	</div>
</div> <!-- #sa-banner-wrapper -->