<?php

// Should we display the off canvas for the main menu per settings? - true by default
$display_off_canvas_main_menu = wp_cache_get( 'display_off_canvas_main_menu', 'sa_framework' );

?><!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<!--[if IE 10]><html class="ie10" lang="en" > <![endif]-->
<html class="no-js<?php echo $display_off_canvas_main_menu ? ' has-off-canvas-menu' : null; ?>" lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php wp_title( '-', true, 'left' ); ?></title>

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

    <a href="#<?php echo apply_filters( 'sa_framework_skip_to_content_id', 'sa-main' ); ?>" id="skip-to-content">Skip to Content</a><?php

    // Start the off canvas main menu
    do_action( 'sa_framework_start_off_canvas_main_menu' );

    ?><div id="sa-wrapper"<?php echo $display_off_canvas_main_menu ? ' class="off-canvas-menu-container"' : null; ?>>

        <div id="sa-inside-wrapper"><?php

            // Print the banner
            do_action( 'sa_framework_print_banner' );

            // Print the header
            do_action( 'sa_framework_print_header' );

            // Print the main menu
            do_action( 'sa_framework_print_main_menu' );

            // Run code before the main area
            do_action( 'sa_framework_before_main_area' );

            // Start the main area
            do_action( 'sa_framework_start_main_area' );