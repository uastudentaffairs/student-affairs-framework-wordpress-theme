(function( $ ) {
    'use strict';

    // Is set to true when the sidebar scroll is in the process of deactivating
    var $sidebar_scroll_is_deactivating = false;

    // Load the global footer
    $( '#sa-global-footer' ).load("https://sa.ua.edu/footer");

    // When the document is ready...
    $(document).ready(function() {

        // Setup items to be sticky
        var $sa_sticky_items = $('.enable-sticky');
        if ( $sa_sticky_items.length > 0 ) {

            // Process each item
            $sa_sticky_items.each(function() {

                // Set item
                var $sa_sticky_item = $(this);

                // When we scroll...
                $(window).scroll(function() {

                    // Which position data are we checking?
                    var $sa_sticky_item_offset_top = ( $sa_sticky_item.data('offset-top') !== undefined ) ? $sa_sticky_item.data('offset-top') : $sa_sticky_item.offset().top;

                    if( $(window).scrollTop() >= $sa_sticky_item_offset_top ) {

                        // Store position and "activate" sticky
                        if ( ! $sa_sticky_item.hasClass('sticky') ) {
                            $sa_sticky_item.data('offset-top', $sa_sticky_item.offset().top).addClass('sticky');
                        }

                    } else {

                        // Reset position and "deactivate" sticky
                        if ( $sa_sticky_item.hasClass('sticky') ) {
                            $sa_sticky_item.data('offset-top', undefined).removeClass('sticky');
                        }

                    }

                });

            });

        }

        // Setup sidebar to "scroll with content"
        var $sa_sidebar_scroll_with_content = $('.sa-sidebar.scroll-with-content');
        if ($sa_sidebar_scroll_with_content.length > 0) {

            // Process each sidebar
            $sa_sidebar_scroll_with_content.each(function() {

                // Set the sidebar
                var $this_sa_sidebar = $(this);

                // Check the sidebar to see if it should scroll
                $this_sa_sidebar.sa_framework_check_sidebar_for_scroll();

                // Check the sidebar when the window is resized
                $( window ).resize(function() {
                    $this_sa_sidebar.sa_framework_check_sidebar_for_scroll();
                });

            });

        }

    });

    ////// FUNCTIONS //////

    // Check the height of the sidebar to see if it should be active or not
    // This function is invoked by the sidebar - returns true if activated, false if not
    $.fn.sa_framework_check_sidebar_for_scroll = function() {

        // Set the sidebar
        var $this_sa_sidebar = $(this);

        // If window is "small", deactivate
        // @TODO intermingle with settings?
        var $screen_is_too_small = false;
        if ( $(window).width() < 604 ) {
            $screen_is_too_small = true;
            $this_sa_sidebar.sa_framework_deactivate_sidebar_scroll();
        }

        // Set some info
        var $window_height = $(window).height();
        var $window_scroll_top = $(window).scrollTop();
        var $sa_main_height = $('#sa-main').height();
        var $sa_main_offset_top = $('#sa-main').offset().top;

        // See how much of the main area is in view on the screen - by default set as if we haven't scrolled at all
        var $sa_main_in_view = $window_height - $window_scroll_top - $sa_main_offset_top;

        // If main area is shorter than the window height, decrement the difference
        if ( $sa_main_height < $window_height ) {
            $sa_main_in_view -= ( $window_height - $sa_main_height );
        }

        // If we've scrolled but the top of the main area is still visible, increment what we've scrolled
        if ( $window_scroll_top > 0 && $window_scroll_top < $sa_main_offset_top ) {
            $sa_main_in_view += $window_scroll_top;
        }

        // Means the top of the main area is at the top of the window or scrolled/hidden above
        else if ( $window_scroll_top > 0 && $window_scroll_top >= $sa_main_offset_top ) {
            $sa_main_in_view = $sa_main_height - $window_scroll_top;
        }

        // If main area is taller than the window, adjust the height
        if ( $sa_main_in_view > $window_height ) {
            $sa_main_in_view = $window_height;
        }

        // Only "activate" if sidebar is shorter than viewable main area
        if ( $this_sa_sidebar.outerHeight() >= $sa_main_in_view ) {

            // Deactivate the sidebar scroll
            if ( $this_sa_sidebar.sa_framework_deactivate_sidebar_scroll() ) {

                // Means it was deactivated
                return false;

            }

        } else if ( ! $screen_is_too_small ) {

            // Activate the sidebar scroll
            if ( $this_sa_sidebar.sa_framework_activate_sidebar_scroll() ) {

                // Means it was activated
                return true;

            }

        }

        return false;

    }

    // "Deactivate" the sidebar scroll
    // This function is invoked by the sidebar - returns true if deactivated, false if not
    $.fn.sa_framework_deactivate_sidebar_scroll = function() {

        // Set the sidebar
        var $this_sa_sidebar = $(this);

        // If it's already deactivated, get out of here
        if ( ! $this_sa_sidebar.hasClass( 'active-scroll-with-content' ) ) {
            return true;
        }

        // Set to true during deactivation process so scroll event won't keep keepin on
        $sidebar_scroll_is_deactivating = true;

        // Send the sidebar back to the top
        $this_sa_sidebar.children('.sa-sidebar-scroll-wrap').stop( true, false ).animate({
            'top': 0
        }, {
            duration: 900,
            easing: 'easeOutCubic',
            complete: function() {

                // "Deactivate" the sidebar
                $this_sa_sidebar.removeClass('active-scroll-with-content').removeAttr( 'style' );

                // Copy the children of the scroll wrap and remove the scroll wrap
                var $scroll_wrap = $this_sa_sidebar.children( '.sa-sidebar-scroll-wrap');
                var $scroll_wrap_children = $scroll_wrap.children().clone();

                // Remove the scroll wrap
                $scroll_wrap.remove();

                // Add children to sidebar
                $scroll_wrap_children.appendTo( $this_sa_sidebar );

                // No longer deactivating
                $sidebar_scroll_is_deactivating = false;

            }
        });

        return true;

    }

    // "Activate" the sidebar scroll
    // This function is invoked by the sidebar - returns true if activated, false if not
    $.fn.sa_framework_activate_sidebar_scroll = function() {

        // Set the sidebar
        var $this_sa_sidebar = $(this);

        // If it's already activated, get out of here
        if ( $this_sa_sidebar.hasClass( 'active-scroll-with-content' ) ) {
            return true;
        }

        // Get the sidebar height and top padding
        var $this_sa_sidebar_height = $this_sa_sidebar.outerHeight();
        var $this_sa_sidebar_padding = $this_sa_sidebar.css('padding');

        // "Activate" the scroll
        $this_sa_sidebar.addClass('active-scroll-with-content');

        // Wrap the sidebars and pass on top padding and set minimum height on the sidebar to match its innards
        $this_sa_sidebar.wrapInner($('<div class="sa-sidebar-scroll-wrap"></div>').css({'padding': $this_sa_sidebar_padding})).css({'min-height': $this_sa_sidebar_height});

        // Set the sidebar when the page first loads
        $this_sa_sidebar.sa_framework_set_sidebar_scroll_with_content( true, 900, 'easeOutCubic' );

        // We have to obviously watch as the window scrolls
        var $scroll_count = 0;
        $(window).scroll(function() {

            // Don't do if in the process of deactivating
            if ( $sidebar_scroll_is_deactivating ) {
                return false;
            }

            // Go ahead and set the sidebar if they scroll for a certain length of time
            if ($scroll_count > 30) {

                // Reset the count
                $scroll_count = 0;

                // Set the sidebar
                $this_sa_sidebar.sa_framework_set_sidebar_scroll_with_content(true, 900, 'linear');

            }

            // Will run when the scroll stops
            clearTimeout($.data(this, 'scroll_has_stopped'));
            $.data(this, 'scroll_has_stopped', setTimeout(function() {

                // Reset the count
                $scroll_count = 0;

                // Set the sidebar
                $this_sa_sidebar.sa_framework_set_sidebar_scroll_with_content(true, 700, 'easeOutCubic');

            }, 100));

            // Increment the timer
            $scroll_count++;

        });

        return true;

    }

    // Sets the top of the sidebar to align with the top of the content
    // This function is invoked by the sidebar
    $.fn.sa_framework_set_sidebar_scroll_with_content = function( $animate, $duration, $easing ) {

        // Make sure it's supposed to scroll
        if ( ! $(this).hasClass( 'scroll-with-content' ) ) {
            return false;
        }

        // Make sure animate is set - default is true
        if ( $animate === undefined && $animate !== true && $animate !== false ) {
            $animate = true;
        }

        // Make sure duration is set - default is 900
        if ( $duration === undefined || ! ( $duration > 0 ) ) {
            $duration = 900;
        }

        // Make sure easing is set - default is linear
        if ( $easing === undefined ) {
            $easing = 'linear';
        }

        // Set the sidebar
        var $this_sa_sidebar = $(this);

        // If we've scrolled past the top of the sidebar
        if ( $(window).scrollTop() > $this_sa_sidebar.offset().top ) {

            if ( $animate ) {

                $this_sa_sidebar.children('.sa-sidebar-scroll-wrap').stop( true, false ).animate({
                    'top': $(window).scrollTop() - $this_sa_sidebar.offset().top
                }, {
                    duration: $duration,
                    easing: $easing
                });

            } else {

                $this_sa_sidebar.children('.sa-sidebar-scroll-wrap').stop( true, false ).css({ 'top': ( $(window).scrollTop() - $this_sa_sidebar.offset().top ) });

            }

        }

        // Otherwise, reset the sidebar
        else {

            if ( $animate ) {

                $this_sa_sidebar.children('.sa-sidebar-scroll-wrap').stop( true, false ).animate({
                    'top': 0
                }, {
                    duration: $duration,
                    easing: $easing
                });

            } else {

                $this_sa_sidebar.children('.sa-sidebar-scroll-wrap').stop( true, false ).css({ 'top': 0 });

            }

        }

    }

})( jQuery );