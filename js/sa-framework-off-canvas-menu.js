(function( $ ) {
    'use strict';

    // Will hold the wrapper which holds EVERYTHING including the off canvas menu and the content
    var $sa_wrapper = null;

    // When the document is ready...
    $(document).ready(function() {

        // Setup any off canvas menus
        var $sa_off_canvas_menus = $( '.enable-off-canvas-menu' );
        if ( $sa_off_canvas_menus.length > 0 ) {

            // Holds EVERYTHING including the off canvas menu and the content
            $sa_wrapper = jQuery( '#sa-wrapper' );

            // Holds the menu
            //var $off_canvas_menu_wrapper = jQuery( '#sa-off-canvas-main-menu-wrapper' );

            // Pushes the content away from the menu
            //var $pusher = jQuery( '#sa-off-canvas-menu-pusher' );

            // Will hold open off canvas menu toggle identifier
            //var $open_listener_toggle = null;

            // Setup the off canvas menu levels
            //setup_ua_eng_off_canvas_menu_levels();

            // Add listeners to open the off canvas menu
            //add_open_ua_eng_off_canvas_menu_listener();

            // Process each menu
            $sa_off_canvas_menus.each(function() {

                // Set the menu
                var $this_menu = $(this);

                // Add listener to all elements who have the class to open the off canvas menu
                $this_menu.find( '.open-sa-off-canvas-menu' ).on( 'touchstart click', function( $event ) {

                    // Set the listener
                    var $this_listener = $(this);

                    // Stop stuff from happening
                    $event.stopPropagation();
                    $event.preventDefault();

                    // Get the off canvas menu
                    var $this_off_canvas_menu = $( '#' + $this_listener.data('menu') );
                    if ( $this_off_canvas_menu.length < 1 ) {
                        return false;
                    }

                    // If the off canvas menu is already open
                    if ( $this_off_canvas_menu.hasClass( 'open' ) ) {

                        // Remove the html listener
                        $('html.' + $this_listener.data('html')).off( 'touchstart click' );

                        // Remove class from the html
                        $('html').removeClass( $this_listener.data('html') );

                        // "Close" the menu
                        $this_off_canvas_menu.removeClass( 'open' );

                        // Remove class from the listener
                        $this_listener.removeClass( 'off-canvas-menu-is-open' );

                        // Remove class from the enable
                        $this_menu.removeClass( 'off-canvas-menu-is-open' );

                    }

                    // Otherwise if its closed...
                    else {

                        // "Open" the menu
                        $this_off_canvas_menu.addClass( 'open' );

                        // Add class to the listener
                        $this_listener.addClass( 'off-canvas-menu-is-open' );

                        // Add class to the enabler
                        $this_menu.addClass( 'off-canvas-menu-is-open' );

                        // If we have a html class...
                        if ( $this_listener.data('html') != '' ) {

                            // Add class to the html
                            $('html').addClass($this_listener.data('html'));

                            // Setup so if html is clicked, it closes
                            $('html.' + $this_listener.data('html')).on( 'touchstart click', function( $event ) {

                                // Don't run inside the menu
                                if ( $($event.target).attr( 'id' ) == $this_off_canvas_menu.attr('id') ) {
                                    return true;
                                }

                                if ( $($event.target).closest( '#'+$this_off_canvas_menu.attr('id')).length > 0 ) {
                                    return true;
                                }

                                // Stop stuff from happening
                                $event.stopPropagation();
                                $event.preventDefault();

                                // Remove the html listener
                                $('html.' + $this_listener.data('html')).off( 'touchstart click' );

                                // Remove class from the html
                                $('html').removeClass( $this_listener.data('html') );

                                // "Close" the menu
                                $this_off_canvas_menu.removeClass( 'open' );

                                // Remove class from the listener
                                $this_listener.removeClass( 'off-canvas-menu-is-open' );

                                // Remove class from the enable
                                $this_menu.removeClass( 'off-canvas-menu-is-open' );

                            });

                        }

                    }

                });

            });

        }

    });

    ////// FUNCTIONS //////

    //! Adds listeners to open the off canvas menu
    /*function add_open_ua_eng_off_canvas_menu_listener() {

        // Add listener to all elements who have the class to open the off canvas menu
        jQuery( '.open-sa-off-canvas-menu' ).on( 'touchstart click', function( $event ) {

            // Stop stuff from happening
            $event.stopPropagation();
            $event.preventDefault();

            // Add class to the listener
            jQuery( '.open-sa-off-canvas-menu' ).addClass( 'off-canvas-menu-open' );

            // If we have a toggle set, add class to the toggle
            $open_listener_toggle = jQuery( this ).data( 'toggle' );
            if ( $open_listener_toggle !== undefined && $open_listener_toggle != '' )
                jQuery( '#' + $open_listener_toggle ).addClass( 'off-canvas-menu-open' );

            // Open the menu
            open_ua_eng_off_canvas_menu();

        });

    }

    //! Removes listeners to open the off canvas menu
    function remove_open_ua_eng_off_canvas_menu_listener() {
        jQuery( '.open-sa-off-canvas-menu' ).off( 'touchstart click' );
    }

    //! Is the off canvas menu open?
    function is_ua_eng_off_canvas_menu_open() {
        return $sa_wrapper.hasClass( 'off-canvas-menu-open' );
    }

    //! Setup the off canvas menu levels
    function setup_ua_eng_off_canvas_menu_levels() {

        // Remember the height of the tallest submenu so we can make sure
        // children are taller than their parents
        var $tallest_submenu = 0;

        // First set it as the top level menu height
        $tallest_submenu = jQuery( '#sa-off-canvas-main-menu' ).height();

        // Go through each submenu and set it up
        jQuery( '#sa-off-canvas-main-menu li.menu-item-has-children' ).each( function( $index ) {

            // Clone the <a> so we can duplicate it in the submenu
            var $menu_item_a = jQuery( this ).children( 'a' ).clone();

            // Define the submenu
            var $submenu = jQuery( this ).children( 'ul.sub-menu' );

            // Create the submenu header and add the menu item
            var $submenu_header = jQuery( '<li class="sub-menu-header"></li>' ).append( $menu_item_a );

            // Add the submenu header
            $submenu.prepend( $submenu_header );

            // Add the back button to the submenu
            $submenu.prepend( '<li class="sub-menu-back close-sub-menu">Back</li>' );

            // Figure out the best height - either height of document or menu, whichever is taller
            var $submenu_height = $submenu.height() > jQuery( document ).height() ? $submenu.height() : jQuery( document ).height();

            // Make sure we keep up with the tallest submenu
            if ( $submenu_height > $tallest_submenu ) {

                $tallest_submenu = $submenu_height;

                // And make sure all of our submenus are tall enough
            } else {

                $submenu_height = $tallest_submenu;

            }

            // Set submenu height
            $submenu.css({ 'height': $submenu_height + 'px' });

        });

    }

    //! Enable the off canvas menu levels
    function enable_ua_eng_off_canvas_menu_levels() {

        // These are the submenu triggers
        jQuery( '#sa-off-canvas-main-menu li.menu-item-has-children > a' ).on( 'touchstart click', function( $event ) {

            // Stop stuff from happening
            $event.stopPropagation();
            $event.preventDefault();

            // Define the submenu
            var $sub_menu = jQuery( this ).next( '.sub-menu' );

            // Open this item's submenu
            $sub_menu.addClass( 'open-sub-menu' );

            // Enable the submenu's back/close button(s)
            $sub_menu.find( '.close-sub-menu' ).on( 'touchstart click', function( $close_event ) {

                // Close this submenu
                jQuery( this ).closest( '.sub-menu' ).removeClass( 'open-sub-menu' );

                // Close all of its children submenus
                jQuery( this ).closest( '.sub-menu' ).find( '.sub-menu' ).removeClass( 'open-sub-menu' );

            });

        });

    }

    //! Open the off canvas menu
    function open_ua_eng_off_canvas_menu() {

        // No point if its already open
        if ( is_ua_eng_off_canvas_menu_open() )
            return false;

        // Remove open menu listener
        remove_open_ua_eng_off_canvas_menu_listener();

        // Clear container class
        $sa_wrapper.attr( 'class', '' );

        // Enable the off canvas menu levels
        enable_ua_eng_off_canvas_menu_levels();

        // Put a tiny delay on the menu opening
        setTimeout( function() {

            // Set it as open
            $sa_wrapper.addClass( 'off-canvas-menu-open' );

            // Will only close the menu if set to true
            var $enable_close_menu = true;

            // This will stop "close menu" actions if you click anywhere in the menu
            $off_canvas_menu_wrapper.on( 'touchstart click', function( $event ) {

                // Don't close the menu if touch inside the menu
                $enable_close_menu = false;

            });

            // This will enable the "close menu" by clicking outside the menu
            $pusher.on( 'touchstart click', function( $event ) {

                // Stop stuff from happening
                $event.preventDefault();

                // Enable closing of the menu
                $enable_close_menu = true;

            });

            // Now that the menu is open, we need to know when someone clicks outside the menu
            jQuery( document ).on( 'touchstart click', function( $event ) {

                // Close the menu if its open
                if ( $enable_close_menu && is_ua_eng_off_canvas_menu_open() ) {

                    // Stop stuff from happening
                    $event.preventDefault();

                    // The menu is closing
                    $enable_close_menu = false;

                    // Close the menu
                    close_ua_eng_off_canvas_menu();

                    // Now that they've clicked, and the menu is closing, unbind these events
                    jQuery( document ).off( 'touchstart click' );
                    $off_canvas_menu_wrapper.off( 'touchstart click' );
                    $pusher.off( 'touchstart click' );

                }

            });

        }, 25 );

    }

    //! Close the off canvas menu
    function close_ua_eng_off_canvas_menu() {

        // Remove all classes, especially 'off-canvas-menu-open'
        $sa_wrapper.attr( 'class', '' );

        // Remove class from triggers
        jQuery( '.open-sa-off-canvas-menu' ).removeClass( 'off-canvas-menu-open' );

        // Remove class from toggle
        if ( $open_listener_toggle !== undefined && $open_listener_toggle != '' )
            jQuery( '#' + $open_listener_toggle ).removeClass( 'off-canvas-menu-open' );

        // Add open canvas menu listener
        add_open_ua_eng_off_canvas_menu_listener();

    }*/

})( jQuery );