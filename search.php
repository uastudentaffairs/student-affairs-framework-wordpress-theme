<?php

// Set the image
/*add_filter( 'sa_framework_hero_image_source', function( $hero_image_src ) {
	return 'https://sa.ua.edu/wp-content/uploads/Hero_general-Camp1831.png';
});*/

// Filter the browser title
/*add_filter( 'wp_title', function( $wp_title, $sep, $seplocation ) {
	return 'Search' . $wp_title;
}, 110, 3 );*/

// Filter the OG site name and title and twitter title for WordPress SEO plugin
add_filter( 'wpseo_opengraph_site_name', 'sa_framework_search_filter_wpseo_title', 110 );
add_filter( 'wpseo_opengraph_title', 'sa_framework_search_filter_wpseo_title', 110 );
add_filter( 'wpseo_twitter_title', 'sa_framework_search_filter_wpseo_title', 110 );
function sa_framework_search_filter_wpseo_title( $title ) {
	return 'Search' . $title;
}

// Set the page title
add_filter( 'sa_framework_section_header', function( $page_section_title ) {
	return 'Search ' . ( is_sa_main_site() ? 'Student Affairs' : ( ( $site_title = get_bloginfo( 'name' ) ) ? $site_title : 'the site' ) );
});

// No permalink
add_filter( 'sa_framework_section_header_permalink', function( $page_section_permalink ) {
	return '';
});

// Remove main loop
remove_action( 'sa_framework_print_main_loop', 'sa_framework_print_main_loop' );

// Print search engine
add_action( 'sa_framework_after_content_area', function() {

	?><script>
		(function() {
			var cx = '014059076202054157577:uzjadxrfjey';
			var gcse = document.createElement('script');
			gcse.type = 'text/javascript';
			gcse.async = true;
			gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//cse.google.com/cse.js?cx=' + cx;
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(gcse, s);
		})();
	</script>
	<gcse:search gname="uasasearch" linkTarget="" safeSearch="active" queryParameterName="s"<?php echo ! is_sa_main_site() ? ' as_sitesearch="' . get_sa_domain() . '"' : null; ?>></gcse:search><?php
	// enableAutoComplete="true"

});

get_header();

get_footer();