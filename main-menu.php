<?php

// Should we display the main menu per settings? - true by default
$display_main_menu = wp_cache_get( 'display_main_menu', 'sa_framework' );

// Display main menu
if ( $display_main_menu ) {

	// Build menu args
	$main_menu_args = array(
		'theme_location'    => 'sa-main',
		'container'         => false,
		'menu_class'        => 'menu main-menu',
		'menu_id'           => 'sa-main-menu',
		'echo'              => true,
		'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'link_before'       => '<span>',
		'link_after'        => '</span>',
		'depth'             => 0,
		'echo'              => false,
	);

	// Get menu
	if ( $main_menu = wp_nav_menu( $main_menu_args ) ) {

		do_action( 'sa_framework_before_main_menu_wrapper', $main_menu_args );

		// Should we display the off canvas for the main menu per settings? - true by default
		$display_off_canvas_main_menu = wp_cache_get( 'display_off_canvas_main_menu', 'sa_framework' );

		?><div id="sa-main-menu-wrapper"<?php echo $display_off_canvas_main_menu ? ' class="enable-off-canvas-menu"' : null; ?>>
			<div class="row">
				<div class="small-12 columns"><?php

					// Display off canvas main menu
					if ( $display_off_canvas_main_menu ) {

						?><div id="sa-main-menu-toggle" class="open-sa-off-canvas-menu" data-html="off-canvas-menu-open" data-menu="sa-off-canvas-main-menu-wrapper">
							<div class="toggle-icon">
								<div class="bar one"></div>
								<div class="bar two"></div>
								<div class="bar three"></div>
							</div>
							<div class="menu-label">Menu</div>
							<div class="menu-label-close">Close</div>
						</div><?php

					}

					// Filter the main menu args
					$main_menu_args = apply_filters( 'sa_framework_main_menu_args', $main_menu_args );

					do_action( 'sa_framework_before_main_menu', $main_menu_args );

					// Print the menu
					echo $main_menu;

					do_action( 'sa_framework_after_main_menu', $main_menu_args );

				?></div>
			</div>
		</div> <!-- #sa-main-menu-wrapper --><?php

		do_action( 'sa_framework_after_main_menu_wrapper', $main_menu_args );

	}

}