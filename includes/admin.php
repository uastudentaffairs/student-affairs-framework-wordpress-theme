<?php

// Hide the Posts menu item
add_action( 'admin_menu', function() {
	remove_menu_page( 'edit.php' );
});

// Remove admin hooks
add_action( 'admin_init', function() {

	// Remove WordPress admin columns if you don't have capability to see them
	if ( isset( $GLOBALS[ 'wpseo_metabox' ] ) && ! current_user_can( 'view_wp_seo_admin_columns' ) ) {

		// This removes the column filter
		remove_action( 'restrict_manage_posts', array( $GLOBALS[ 'wpseo_metabox' ], 'posts_filter_dropdown' ) );

		// This removes the columns
		// We have to call by each individual post type because that's how they do it
		// and if we don't, their filter comes later and will overwrite our filter.
		if ( ( $post_types = get_post_types( array( 'public' => true ), 'names' ) )
		     && is_array( $post_types ) ) {

			foreach ( $post_types as $pt ) {
				remove_filter( 'manage_' . $pt . '_posts_columns', array( $GLOBALS[ 'wpseo_metabox' ], 'column_heading' ) );
			}

		}

	}

}, 100000 );

// Setup admin styles and scripts
add_action( 'admin_enqueue_scripts', function ( $hook_suffix ) {
	global $wp_scripts;

	// If can't view Yoast meta box, remove Yoast scripts
	if ( ! current_user_can( 'view_wp_seo_meta_box' ) ) {
		wp_dequeue_script( 'yoast-seo' );
		unset( $wp_scripts->queue[ 'yoast-seo' ] );
		wp_dequeue_script( 'wp-seo-metabox' );
		unset( $wp_scripts->queue[ 'wp-seo-metabox' ] );
		wp_dequeue_script( 'wpseo-admin-global-script' );
		unset( $wp_scripts->queue[ 'wpseo-admin-global-script' ] );
		wp_dequeue_script( 'wpseo-admin-media' );
		unset( $wp_scripts->queue[ 'wpseo-admin-media' ] );
		wp_dequeue_script( 'wp-seo-post-scraper' );
		unset( $wp_scripts->queue[ 'wp-seo-post-scraper' ] );
		wp_dequeue_script( 'wp-seo-replacevar-plugin' );
		unset( $wp_scripts->queue[ 'wp-seo-replacevar-plugin' ] );
		wp_dequeue_script( 'wp-seo-shortcode-plugin' );
		unset( $wp_scripts->queue[ 'wp-seo-shortcode-plugin' ] );
		wp_dequeue_script( 'wp-seo-featured-image' );
		unset( $wp_scripts->queue[ 'wp-seo-featured-image' ] );
	}

}, 100000000000 );

// Add meta boxes
add_action( 'add_meta_boxes', function () {

	/*// Add content layout meta boxes - if user has the capability
	if ( current_user_can( 'edit_sa_content_layout' ) ) {

		$layout_post_types = array( 'page' );
		foreach ( $layout_post_types as $post_type ) {
			add_meta_box( 'sa-framework-layout', __( 'Content Layout', 'uastudentaffairs' ), 'print_sa_framework_meta_boxes', $post_type, 'side', 'core' );
		}

	}*/

});

// Remove meta boxes
add_action( 'add_meta_boxes', function ( $post_type, $post ) {

	// If you don't have the capability to edit WordPress SEO, remove the meta box
	if ( ! current_user_can( 'view_wp_seo_meta_box' ) ) {
		remove_meta_box( 'wpseo_meta', $post_type, 'normal' );
	}

	// Remove "Content Audit Notes" meta box. We'll use the Edit Flow comments instead.
	if ( $options = get_option( 'content_audit' ) ) {
		foreach ( $options[ 'post_types' ] as $content_type ) {
			remove_meta_box( 'content_audit_meta', $content_type, 'normal' );
		}
	}

}, 1000, 2 );

// Print meta boxes
function print_sa_framework_meta_boxes( $post, $metabox ) {

	/*switch( $metabox[ 'id' ] ) {

		case 'sa-framework-layout':

			$layout_options = array( '' );

			?><label class="screen-reader-text" for="sa_framework_layout">Content Layout</label>
			<select name="sa_framework_layout" id="sa_framework_layout" style="width: 100%;">
				<option value="2" selected="selected">Rachel Carden</option>
			</select><?php

			break;

	}*/

}

// Clean up the page template list
add_filter( 'theme_page_templates', function( $page_templates, $theme, $post ) {
	global $blog_id;

	// Remove templates that should only be on the main site
	if ( ! is_main_site( $blog_id ) ) {
		unset( $page_templates[ 'template-departments.php' ] );
		unset( $page_templates[ 'template-staff-directory.php' ] );
	}

	return $page_templates;
}, 100, 3 );

// Tweak Edit Flow arguments to get all users
add_filter( 'ef_users_select_form_get_users_args', function( $args ) {

	// Unset the who filter
	unset( $args[ 'who' ] );

	return $args;
}, 1000 );

// Add options page and field groups
add_action( 'admin_menu', function() {

	// Use the ACF plugin for ease
	if ( function_exists( 'acf_add_options_page' ) ) {

		// Add the options page
		acf_add_options_page( array(
			'page_title' => __( 'Student Affairs Settings', 'uastudentaffairs' ),
			'menu_title' => __( 'Student Affairs Settings', 'uastudentaffairs' ),
			'menu_slug'  => 'student-affairs-framework',
			'capability' => 'manage_options',
		) );

	}

	// Add field groups
	if ( function_exists('acf_add_local_field_group') ) :

		// These are the layout choices for the fields
		$layout_choices = array(
			'1 Full Width Column'   => '1 Full Width Column',
			'Left Sidebar'          => 'Left Sidebar',
			'Right Sidebar'         => 'Right Sidebar',
		);

		// Get menu choices
		$menu_choices = array();
		if ( $menus = wp_get_nav_menus() ) {
			foreach( $menus as $menu ) {
				$menu_choices[ $menu->term_id ] = $menu->name;
			}
		}

		// Add main Student Affairs settings
		acf_add_local_field_group(array (
			'key' => 'group_55c21dc3b051a',
			'title' => 'General Settings',
			'fields' => array (
				array (
					'key' => 'field_55c21dc821b4f',
					'label' => 'What is the slug for this site?',
					'name' => 'sa_framework_site_slug',
					'type' => 'text',
					'instructions' => 'Always the same as the subdomain, e.g. the slug for Office of Fraternity and Sorority Life is <em>ofsl</em>.',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => function_exists( 'get_sa_subdomain_slug' ) ? get_sa_subdomain_slug() : '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'student-affairs-framework',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
		));

		// Add banner settings
		acf_add_local_field_group(array (
			'key' => 'group_55dbb31780676',
			'title' => 'Banner',
			'fields' => array (
				array (
					'key' => 'field_55dbb3ac1da82',
					'label' => 'If needed, provide a shorter breadcrumb label for the current site on small screens.',
					'name' => 'sa_framework_banner_breadcrumbs_site_small_label',
					'type' => 'text',
					'instructions' => '',
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'student-affairs-framework',
					),
				),
			),
			'menu_order' => 2,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		// Add the Grid Lines setting
		acf_add_local_field_group(array (
			'key' => 'group_56266e84ea1cb',
			'title' => 'Grid Lines',
			'fields' => array (
				array (
					'key' => 'field_56266edcb6640',
					'label' => 'Do you want to display the theme grid lines?',
					'name' => 'sa_framework_display_grid_lines',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'student-affairs-framework',
					),
					array (
						'param' => 'current_user_role',
						'operator' => '==',
						'value' => 'administrator',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		// Add "Main Menu" field group for options page
		acf_add_local_field_group(array (
			'key' => 'group_55b68214c0652',
			'title' => 'Main Menu',
			'fields' => array (
				array (
					'key' => 'field_55b682192eb14',
					'label' => 'Do you want to display the main menu?',
					'name' => 'sa_framework_display_main_menu',
					'type' => 'radio',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'Yes' => 'Yes',
						'No' => 'No',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'Yes',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_55d75bdd8f2cf',
					'label' => 'Do you want to include the off-canvas main menu?',
					'name' => 'sa_framework_display_off_canvas_main_menu',
					'type' => 'radio',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_55b682192eb14',
								'operator' => '==',
								'value' => 'Yes',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'Yes' => 'Yes',
						'No' => 'No',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'Yes',
					'layout' => 'horizontal',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'student-affairs-framework',
					),
				),
			),
			'menu_order' => 5,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
		));

		// Add "Layout" field group for options page
		acf_add_local_field_group(array (
			'key' => 'group_5589d17b98ad0',
			'title' => 'Layout',
			'fields' => array (
				array (
					'key' => 'field_5589d2307438b',
					'label' => 'Default Page Layout',
					'name' => 'sa_framework_default_page_layout',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $layout_choices,
					'default_value' => array (
						'1 Full Width Column' => '1 Full Width Column',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'placeholder' => '',
					'disabled' => 0,
					'readonly' => 0,
				),
				array (
					'key' => 'field_55c2198369215',
					'label' => 'Do you want this sidebar to display a submenu?',
					'name' => 'sa_framework_default_display_page_submenu',
					'type' => 'radio',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5589d2307438b',
								'operator' => '==',
								'value' => 'Left Sidebar',
							),
						),
						array (
							array (
								'field' => 'field_5589d2307438b',
								'operator' => '==',
								'value' => 'Right Sidebar',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'Yes' => 'Yes',
						'No' => 'No',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'Yes',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_55c21b4fa976f',
					'label' => 'Do you want this sidebar to display a custom submenu?',
					'name' => 'sa_framework_default_display_custom_page_submenu',
					'type' => 'radio',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5589d2307438b',
								'operator' => '==',
								'value' => 'Left Sidebar',
							),
						),
						array (
							array (
								'field' => 'field_5589d2307438b',
								'operator' => '==',
								'value' => 'Right Sidebar',
							),
						),
						array (
							array (
								'field' => 'field_55c2198369215',
								'operator' => '==',
								'value' => 'Yes',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'Yes' => 'Yes',
						'No' => 'No',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'No',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_55c21f3291556',
					'label' => 'Select the submenu',
					'name' => 'sa_framework_default_page_submenu',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_55c21b4fa976f',
								'operator' => '==',
								'value' => 'Yes',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $menu_choices,
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'placeholder' => '',
					'disabled' => 0,
					'readonly' => 0,
				),
				array (
					'key' => 'field_5589d18d590b2',
					'label' => 'Default Front Page Layout',
					'name' => 'sa_framework_default_front_page_layout',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $layout_choices,
					'default_value' => array (
						'1 Full Width Column' => '1 Full Width Column',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'placeholder' => '',
					'disabled' => 0,
					'readonly' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'student-affairs-framework',
					),
				),
			),
			'menu_order' => 5,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
		));

		// Add "Page Layout" group to post type screens - but not front page
		acf_add_local_field_group(array (
			'key' => 'group_5589d4291f8a3',
			'title' => 'Page Layout',
			'fields' => array (
				array (
					'key' => 'field_5589d4292b6c0',
					'label' => 'Page Layout',
					'name' => 'sa_framework_page_layout',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array_merge( array( 'Default' => 'Use Default Page Layout' ), $layout_choices ),
					'default_value' => array (
						'Default' => 'Use Default Page Layout',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'placeholder' => '',
					'disabled' => 0,
					'readonly' => 0,
				),
				array (
					'key' => 'field_55ad1ab756663',
					'label' => 'Do you want this sidebar to display a submenu?',
					'name' => 'sa_framework_display_page_submenu',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5589d4292b6c0',
								'operator' => '==',
								'value' => 'Left Sidebar',
							),
						),
						array (
							array (
								'field' => 'field_5589d4292b6c0',
								'operator' => '==',
								'value' => 'Right Sidebar',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'Default' => 'Use Default Setting',
						'Yes' => 'Yes',
						'No' => 'No',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'Use Default Setting',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_55ad4c9eade31',
					'label' => 'Do you want this page to display a custom submenu?',
					'name' => 'sa_framework_display_custom_page_submenu',
					'type' => 'radio',
					'instructions' => 'This will overwrite the default behavior which pulls the submenu from the main menu.',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								array (
									'field' => 'field_5589d4292b6c0',
									'operator' => '==',
									'value' => 'Left Sidebar',
								),
							),
							array (
								array (
									'field' => 'field_5589d4292b6c0',
									'operator' => '==',
									'value' => 'Right Sidebar',
								),
							),
							array (
								'field' => 'field_55ad1ab756663',
								'operator' => '==',
								'value' => 'Yes',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'Yes' => 'Yes',
						'No' => 'No',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'No',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_55ad4cd3ade32',
					'label' => 'Select the submenu',
					'name' => 'sa_framework_page_submenu',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_55ad4c9eade31',
								'operator' => '==',
								'value' => 'Yes',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $menu_choices,
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'placeholder' => '',
					'disabled' => 0,
					'readonly' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
					),
					array (
						'param' => 'current_user_role',
						'operator' => '==',
						'value' => 'administrator',
					),
					array (
						'param' => 'page_type',
						'operator' => '!=',
						'value' => 'front_page',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
		));

		// Add "Breadcrumbs" field group to post type screens - but not front page
		acf_add_local_field_group(array (
			'key' => 'group_559c174b5ea85',
			'title' => 'Breadcrumbs',
			'fields' => array (
				array (
					'key' => 'field_559c175022a56',
					'label' => 'Do you want to display the page breadcrumbs?',
					'name' => 'sa_framework_display_page_breadcrumbs',
					'type' => 'radio',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'Yes' => 'Yes',
						'No' => 'No',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'Yes',
					'layout' => 'horizontal',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
					),
					array (
						'param' => 'current_user_role',
						'operator' => '==',
						'value' => 'administrator',
					),
					array (
						'param' => 'page_type',
						'operator' => '!=',
						'value' => 'front_page',
					),
				),
			),
			'menu_order' => 1,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
		));

		// Add Social Media Accounts group
		acf_add_local_field_group(array (
			'key' => 'group_55ef3447c13bb',
			'title' => 'Social Media Accounts',
			'fields' => array (
				array (
					'key' => 'field_55ef344dd33be',
					'label' => 'Facebook URL',
					'name' => 'facebook_url',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
				),
				array (
					'key' => 'field_55ef34bcd33bf',
					'label' => 'Twitter Handle',
					'name' => 'twitter_handle',
					'type' => 'text',
					'instructions' => '<strong>Do NOT include the @, e.g. uastudentaffairs.</strong>',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_55ef34f2d33c0',
					'label' => 'Instagram Handle',
					'name' => 'instagram_handle',
					'type' => 'text',
					'instructions' => '<strong>Do NOT include the @, e.g. uastudentaffairs.</strong>',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_55ef34f2d33c4',
					'label' => 'Instagram User ID',
					'name' => 'instagram_user_id',
					'type' => 'text',
					'instructions' => '<a href="http://www.otzberg.net/iguserid/" target="_blank">Use this website to get your ID</a>. <strong>You must include this ID in order to retrieve your posts.</strong>',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'student-affairs-framework',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'departments',
					),
				),
			),
			'menu_order' => 5,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		// Add Social Media meta group
		acf_add_local_field_group(array (
			'key' => 'group_55c91e43725c0',
			'title' => 'Social Media Meta Information',
			'fields' => array (
				array (
					'key' => 'field_55c91e6afc3c2',
					'label' => 'Facebook Image',
					'name' => 'sa_facebook_image',
					'type' => 'image',
					'instructions' => 'Use this field if you would like to have an image for Facebook but do not want to display a featured image on the page. <em>If a featured image is set, it will take priority.</em>',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'id',
					'preview_size' => 'medium',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array (
					'key' => 'field_55c91f19fd6d5',
					'label' => 'Twitter Image',
					'name' => 'sa_twitter_image',
					'type' => 'image',
					'instructions' => 'Use this field if you would like to have an image for Twitter but do not want to display a featured image on the page. <em>If a featured image is set, it will take priority.</em>',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'id',
					'preview_size' => 'medium',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
					),
				),
			),
			'menu_order' => 5,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
		));

	endif;

}, 0 );