<?php

// Add our own query vars
add_filter( 'query_vars', function( $vars ) {

	// For our search pages
	$vars[] = 'is_sa_search_page';

	return $vars;
}, 10, 1 );

// Setup rewrite rules
add_action( 'init', function() {

	// Setup search landing page
	add_rewrite_rule( '^search/?', 'index.php?is_sa_search_page=1', 'top' );

});

// Filter which template to show
add_filter( 'template_include', function( $template ) {
	if ( get_query_var( 'is_sa_search_page' ) || is_search() ) {

		// Define search page constant
		define( 'IS_SA_SEARCH_PAGE', true );

		return get_template_directory() . '/search.php';
	} else {
		define( 'IS_SA_SEARCH_PAGE', false );
	}
	return $template;
});