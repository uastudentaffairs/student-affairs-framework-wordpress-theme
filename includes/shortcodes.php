<?php

// We have to remove the default shortcode filter
remove_filter( 'the_content', 'do_shortcode', 11 ); // AFTER wpautop()

// Strips extra space around and within shortcodes
add_filter( 'the_content', function( $content ) {

	// Clean it
	$content = strtr( $content, array( "\n[" => '[', "]\n" => ']', '<p>[' => '[', ']</p>' => ']', ']<br>' => ']', ']<br />' => ']' ) );

	// Return the content
	return do_shortcode( $content );

}, 11 );

//! Include columns in content
add_shortcode( 'columns', function( $args, $content = NULL ) {

	// Make sure there's content to wrap
	if ( ! $content )
		return NULL;

	// Process for more levels of shortcode, wrap in row and return
	return '<div class="row">' . do_shortcode( $content ) . '</div>';

});

//! Include columns in content
add_shortcode( 'col', function( $args, $content = NULL ) {

	// Make sure there's content to wrap
	if ( ! $content )
		return NULL;

	// Process args
	$defaults = array(
		'small' => '12',
		'medium' => false,
		'large' => false,
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_OVERWRITE );

	// Setup column classes
	$column_classes = array();

	foreach( array( 'small', 'medium', 'large' ) as $size ) {

		// If a value was passed, make sure its a number
		if ( isset( ${$size} ) && ! is_numeric( ${$size} ) && ! is_int( ${$size} ) )
			continue;

		// Add the class
		$column_classes[] = "{$size}-${$size}";

	}

	return '<div class="' . implode( ' ', $column_classes ) . ' columns">' . do_shortcode( $content ) . '</div>';

});

//! Wrap email addresses in antispambot()
add_shortcode( 'email', function( $args, $content = null ) {

	// Make sure we have an email
	if ( ! is_email( $content ) ) {
		return;
	}

	// Parse the args
	$args = shortcode_atts( array(
		'icon' => false,
		), $args, 'email' );

	// Build email string
	$email = '<a href="mailto:' . antispambot( $content ) . '"';

		if ( $args[ 'icon' ] !== false ) {
			$email .= ' class="has-icon"><span class="dashicons dashicons-email-alt"></span> <span>' . antispambot( $content ) . '</span>';
		} else {
			$email .= '>' . antispambot( $content );
		}

	$email .= '</a>';

	return $email;
});

//! Print custom fields
add_shortcode( 'print_sa_custom_field', function( $args, $content = null ) {
	global $post;

	// Setup the args
	$defaults = array(
		'post_id'   => $post->ID,
		'meta_key'  => null,
		'single'    => true,
		'wpautop'   => false,
		'do_shortcode' => true,
	);
	$args = wp_parse_args( $args, $defaults );

	// Make sure we have a post ID and meta key
	if ( ! ( $args[ 'post_id' ] > 0 ) || empty( $args[ 'meta_key' ] ) ) {
		return null;
	}

	// Setup single argument - true by default
	if ( ! empty( $args['single'] ) && ( false === $args['single'] || strcasecmp( 'false', $args['single'] ) == 0 ) ) {
		$args['single'] = false;
	} else {
		$args['single'] = true;
	}

	if ( $meta_value = get_post_meta( $args[ 'post_id' ], $args[ 'meta_key' ], $args[ 'single' ] ) ) {

		// Setup do_shortcode argument - true by default
		if ( ! empty( $args['do_shortcode'] ) && ( false === $args['do_shortcode'] || strcasecmp( 'false', $args['do_shortcode'] ) == 0 ) ) {
			$args['do_shortcode'] = false;
		} else {
			$args['do_shortcode'] = true;
		}

		// Setup wpautop argument - false by default
		if ( ! empty( $args['wpautop'] ) && ( true === $args['wpautop'] || $args['wpautop'] > 0 || strcasecmp( 'true', $args['wpautop'] ) == 0 ) ) {
			$args['wpautop'] = true;
		} else {
			$args['wpautop'] = false;
		}

		if ( is_string( $meta_value ) ) {

			if ( $args['do_shortcode'] ) {
				$meta_value = do_shortcode( $meta_value );
			}

			if ( $args['wpautop'] ) {
				return wpautop( $meta_value );
			}

			return $meta_value;

		}

	}

	return null;

});

//! Print ISSUU embeds
add_shortcode( 'print_sa_children_list', function( $args, $content = null ) {
	global $post;

	// Setup the args
	$args = shortcode_atts( array(
		'post_id' => $post->ID,
	), $args, 'print_sa_children_list' );

	// Build content
	$content = null;

	// Set the post
	$the_post = $post;

	// Check the post
	if ( ! ( isset( $the_post ) && isset( $the_post->ID ) && isset( $the_post->post_type ) ) && isset( $args[ 'post_id' ] ) ) {
		$the_post = get_post( $args[ 'post_id' ] );
	}

	// First, get child
	if ( $children = get_children( array(
		'post_parent'   => $the_post->ID,
		'post_type'     => $the_post->post_type,
		'numberposts'   => -1,
		'post_status'   => 'publish',
		'orderby'       => 'menu_order'
	) ) ) {

		// Build list
		$content = '<div class="sa-items">';

		foreach( $children as $child ) {

			// Get the featured image
			$featured_image_src = ( $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $child->ID ), 'full' ) ) && isset( $featured_image_array[0] ) ? $featured_image_array[0] : false;

			// Get the button text
			$button_text = get_post_meta( $child->ID, 'button_text', true );

			// Get the permalink
			$child_permalink = get_permalink( $child->ID );

			// Build item classes
			$item_classes = array( 'sa-item' );

			// If we're adding a button
			if ( $button_text ) {
				$item_classes[] = 'has-bottom-button';
			}

			// Build list item style
			$list_item_style = array();

			// Build featured image background
			if ( $featured_image_src ) {
				$item_classes[] = 'has-thumb';
				$list_item_style[] = 'background-image:url(' . $featured_image_src . ')';
			}

			// Build list item
			$content .= '<div class="' . implode( ' ', $item_classes ) . '"' . ( ! empty( $list_item_style ) ? ' style="' . implode( ' ', $list_item_style ) . '"' : null ) . '>';

			// Add header
			$content .= '<h2 class="item-title"><a href="' . $child_permalink . '">' . get_the_title( $child->ID ) . '</a></h2>';

			// Add content
			$content .= '<div class="item-content">' . wpautop( $child->post_excerpt ) . '</div>';

			// Add the button
			if ( $button_text ) {
				$content .= '<a class="item-bottom-button button secondary expand" href="' . $child_permalink . '">' . $button_text . '</a>';
			}

			// Close list item
			$content .= '</div>';

		}

		// Close list
		$content .= '</div>';

	}

	return $content;

});

//! Print ISSUU embeds
add_shortcode( 'print_sa_issuu', function( $args, $content = null ) {

	// Setup the args
	$defaults = array(
		'config_id'     => null,
		'width'         => null,
		'height'        => 400,
		'download_url'  => null,
	);
	$args = wp_parse_args( $args, $defaults );

	// Make sure we have IDs..
	if ( empty( $args[ 'config_id' ] ) ) {
		return null;
	}

	// Let's get all the IDs into an array
	if ( ! is_array( $args[ 'config_id' ] ) ) {
		$args[ 'config_id' ] = explode( ',', preg_replace( '/\s/i', '', $args[ 'config_id' ] ) );
	}

	// Make sure we still have some IDs
	if ( empty( $args[ 'config_id' ] ) ) {
		return null;
	}

	// Let's get all the download URLS into an array
	if ( ! empty( $args[ 'download_url' ] ) && ! is_array( $args[ 'download_url' ] ) ) {
		$args[ 'download_url' ] = explode( ',', preg_replace( '/\s/i', '', $args[ 'download_url' ] ) );
	}

	// Build content for each embed
	$embeds = array();

	foreach( $args[ 'config_id' ] as $config_id_key => $config_id ) {

		// Build styles
		$this_embed_styles = array();

		if ( ! empty( $args[ 'width' ] ) ) {
			$this_embed_styles[] = 'width:' . ( is_numeric( $args[ 'width' ] ) ? ( $args[ 'width' ] . 'px;' ) : $args[ 'width' ] );
		}

		if ( ! empty( $args[ 'height' ] ) ) {
			$this_embed_styles[] = 'height:' . ( is_numeric( $args[ 'height' ] ) ? ( $args[ 'height' ] . 'px;' ) : $args[ 'height' ] );
		}

		// Build this embed content
		$this_embed = '<div data-configid="' . $config_id . '"' . ( ! empty( $this_embed_styles ) ? ' style="' . implode( ';', $this_embed_styles ) . '"' : null ) . ' class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>';

		// Does this have a download URL?
		if ( $this_embed_download_url = ! empty( $args[ 'download_url' ] ) && isset( $args[ 'download_url' ][ $config_id_key ] ) ? $args[ 'download_url' ][ $config_id_key ] : null ) {
			$this_embed .= '<a class="button expand download" href="' . $this_embed_download_url . '">Download document</a>';
		}

		// Add to embeds
		$embeds[] = '<div class="sa-issuu-embed">' . $this_embed . '</div>';

	}

	// If just one embed, then return as is
	if ( count( $embeds ) == 1 ) {
		return array_shift( $embeds );
	}

	// Otherwise, wrap them in a row
	$content = null;
	foreach( $embeds as $embed ) {
		$content .= '<div class="small-12 medium-6 columns">' . $embed . '</div>';
	}

	// Wrap in a row and return
	return '<div class="row">' . $content . '</div>';

});

//!  Register a UI for the shortcodes
if ( function_exists( 'shortcode_ui_register_for_shortcode' ) ) {

	shortcode_ui_register_for_shortcode(
		'print_sa_custom_field',
		array(
			'label'         => 'Print Student Affairs Custom Field Value',
			'listItemImage' => 'dashicons-media-code',
			'post_type'     => array( 'page' ),
			'attrs'         => array(
				array(
					'label'     => 'Custom Field (Meta) Key',
					'attr'      => 'meta_key',
					'type'      => 'text',
				),
				array(
					'label'     => 'Post ID - If left blank, will use current post.',
					'attr'      => 'post_id',
					'type'      => 'number',
				),
				array(
					'label'     => 'Check if you want to apply wpautop() to the value',
					'attr'      => 'wpautop',
					'type'      => 'checkbox',
				),
			),
		)
	);

	shortcode_ui_register_for_shortcode(
		'print_sa_issuu',
		array(
			'label'         => 'Print Student Affairs ISSUU Documents',
			'listItemImage' => 'dashicons-media-code',
			'post_type'     => array( 'page' ),
			'attrs'         => array(
				array(
					'label'     => 'Config ID - Include multiple embeds by separating the IDs with commas.',
					'attr'      => 'config_id',
					'type'      => 'text',
				),
				array(
					'label'     => 'Width - Can be number or percentage. Default is 100%.',
					'attr'      => 'width',
					'type'      => 'text',
				),
				array(
					'label'     => 'Height - Can be number or percentage. Default is 400.',
					'attr'      => 'height',
					'type'      => 'text',
				),
				array(
					'label'     => 'Download URL - Include if you want users to be able to download the document. For multiple embeds, separate the URLs with commas.',
					'attr'      => 'download_url',
					'type'      => 'text',
				),
			),
		)
	);

}