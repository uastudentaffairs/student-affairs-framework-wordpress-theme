<?php

// Disable XML-RPC
add_filter( 'xmlrpc_enabled', '__return_false' );

// Set the media sizes
add_filter( 'pre_option_thumbnail_size_w', function() { return '300'; });
add_filter( 'pre_option_thumbnail_size_h', function() { return '300'; });
add_filter( 'pre_option_medium_size_w', function() { return '800'; });
add_filter( 'pre_option_large_size_w', function() { return '2000'; });
add_filter( 'pre_option_large_size_h', function() { return '2000'; });
add_filter( 'pre_option_uploads_use_yearmonth_folders', function() { return 0; });

// Filter the <body> class
add_filter( 'body_class', function( $classes, $class ) {
	global $sa_framework_site_slug;

	// Add the site slug
	$classes[] = "sa-{$sa_framework_site_slug}";

	// Get the display main menu setting - true by default
	$display_main_menu = strcasecmp( 'no', get_option( 'options_sa_framework_display_main_menu' ) ) == 0 ? false : true;

	// Filter the "display main menu" setting
	$display_main_menu = apply_filters( 'sa_framework_display_main_menu', $display_main_menu );

	// Display main menu
	if ( $display_main_menu ) {

		// Get the display off canvas main menu setting - true by default
		$display_off_canvas_main_menu = strcasecmp( 'no', get_option( 'options_sa_framework_display_off_canvas_main_menu' ) ) == 0 ? false : true;

		// Filter the "display off canvas main menu" setting
		$display_off_canvas_main_menu = apply_filters( 'sa_framework_display_off_canvas_main_menu', $display_off_canvas_main_menu );

		// Do we have the off canvas menu?
		if ( $display_off_canvas_main_menu ) {
			$classes[] = 'has-off-canvas-menu';
		}

	}

	// Add this page's defined layout
	if ( $defined_page_layout =  wp_cache_get( 'page_layout', 'sa_framework' ) ) {
		$classes[] = 'layout-' . strtolower( preg_replace( '/\s/i', '-', trim( preg_replace( '/[^a-z\s\-]/i', '', $defined_page_layout ) ) ) );
	}

	return $classes;
}, 10, 2 );

// Filter the browser title
add_filter( 'wp_title', function( $wp_title, $sep, $seplocation ) {

	// Add Student Affairs
	// @TODO fix with filter?
	if ( ! is_sa_main_site() ) {
		if ( 'right' == $seplocation ) {
			$wp_title .= "Division of Student Affairs {$sep} ";
		} else {
			$wp_title .= " {$sep} Division of Student Affairs";
		}
	}

	// Add The University of Alabama
	if ( 'right' == $seplocation ) {
		$wp_title .= "The University of Alabama {$sep} ";
	} else {
		$wp_title .= " {$sep} The University of Alabama";
	}

	return $wp_title;

}, 100, 3 );

// Filter the OG site name and title and twitter title for WordPress SEO plugin
add_filter( 'wpseo_opengraph_site_name', 'sa_framework_filter_wpseo_title', 100 );
add_filter( 'wpseo_opengraph_title', 'sa_framework_filter_wpseo_title', 100 );
add_filter( 'wpseo_twitter_title', 'sa_framework_filter_wpseo_title', 100 );
function sa_framework_filter_wpseo_title( $title ) {

	// Add Student Affairs
	// @TODO fix with filter?
	if ( ! is_sa_main_site() ) {
		$title .= ' - Division of Student Affairs';
	}

	// Add The University of Alabama
	$title .= ' - The University of Alabama';

	return $title;

}

// Filter image size for WordPress SEO Facebook OG - default is 'original'
add_filter( 'wpseo_opengraph_image_size', function( $image_size ) {
	return 'sa-facebook';
}, 100 );

// Filter image size for WordPress SEO Twitter - default = 'full'
add_filter( 'wpseo_twitter_image_size', function( $image_size ) {
	return 'sa-twitter';
}, 100 );

// Setup my own action after WordPress SEO to add an OpenGraph image
add_action( 'wpseo_opengraph', function() {
	global $post;

	// We only have to worry about this if the page doesn't have a featured image - they take priority
	if ( isset( $post ) && ! has_post_thumbnail( $post->ID ) ) {

		// Get the image set by our custom field
		if ( ( $image = wp_get_attachment_image_src( get_post_meta( $post->ID, 'sa_facebook_image', true ), apply_filters( 'wpseo_opengraph_image_size', 'original' ) ) )
			&& isset( $image[0] ) ) {

			echo '<meta property="og:image" content="' . $image[0] . '" />' . "\n";

		}

	}

}, 31 );

// Setup my own action to add the Twitter image
add_action( 'wpseo_twitter', function() {
	global $post;

	// We only have to worry about this if the page doesn't have a featured image - they take priority
	if ( isset( $post ) && ! has_post_thumbnail( $post->ID ) ) {

		// Get the image set by our custom field
		if ( ( $image = wp_get_attachment_image_src( get_post_meta( $post->ID, 'sa_twitter_image', true ), apply_filters( 'wpseo_twitter_image_size', 'original' ) ) )
		     && isset( $image[0] ) ) {

			echo '<meta name="twitter:image:src" content="' . $image[0] . '"/>' . "\n";

		}

	}

}, 100 );

// Filter the "excerpt more" - make blank and add in filter below
add_filter( 'excerpt_more', function( $excerpt_more ) {
	return '';
});

// Add ellipses to excerpts
add_filter( 'wp_trim_excerpt', 'sa_framework_add_ellipses_to_excerpt', 9, 2 );
function sa_framework_add_ellipses_to_excerpt( $text, $raw_excerpt ) {

	// Clean up the right end of the text
	$text = rtrim( $text );

	// Add ellipses
	$text .= '&hellip;';

	return $text;
}

// Add "read more" to excerpts
add_filter( 'wp_trim_excerpt', 'sa_framework_add_read_more_to_excerpt', 10, 2 );
function sa_framework_add_read_more_to_excerpt( $text, $raw_excerpt ) {

	// Clean up the right end of the text
	$text = rtrim( $text );

	// Add "Read more" if we have the link
	if ( $permalink = get_permalink() ) {
		$text .= ' <a href="' . $permalink . '">Read more</a>';
	}

	return $text;
}

// Filter the submenu header for search results
add_filter( 'sa_framework_submenu_header', function( $submenu_header, $menu_args ) {

	// Add a submenu header for the search results menu
	if ( isset( $menu_args[ 'theme_location' ] ) && 'sa-search-results' == $menu_args[ 'theme_location' ] ) {
		return 'For More Information';
	}

	return $submenu_header;

}, 10, 2 );

// Filter the menu to get the current menu section
add_filter( 'wp_nav_menu_objects', function( $sorted_menu_items, $menu_args ) {

	// Only for the main menu
	if ( 'sa-main' != $menu_args->theme_location ) {
		return $sorted_menu_items;
	}

	// Get the menu section post ID - no need to run code if already exists
	$menu_section_post_id_found = false;
	$set_menu_section_post_id = wp_cache_get( 'menu_section_post_id', 'sa_framework', false, $menu_section_post_id_found );

	// If already set, get out of here
	if ( $menu_section_post_id_found && $set_menu_section_post_id > 0 ) {
		return $sorted_menu_items;
	}

	// Store menu items in new array indexed by menu ID
	$indexed_menu_items = array();

	// Get/filter the menu section post ID - allows to override
	$menu_section_post_id = apply_filters( 'sa_framework_menu_section_post_id', 0 );

	// Find the current menu item
	if ( ! ( $menu_section_post_id > 0 ) ) {
		foreach ( $sorted_menu_items as $menu_item ) {

			// Index the menu item
			$indexed_menu_items[ $menu_item->ID ] = $menu_item;

			// The current menu item
			if ( $menu_item->current ) {

				// If item has a parent
				if ( isset( $menu_item->menu_item_parent ) && $menu_item->menu_item_parent > 0 ) {

					// Get the parent menu item from the index
					$parent_menu_item = $indexed_menu_items[ $menu_item->menu_item_parent ];

					// If the parent has a parent, keep going!
					while ( isset( $parent_menu_item->menu_item_parent ) && $parent_menu_item->menu_item_parent > 0 ) {
						$parent_menu_item = $indexed_menu_items[ $parent_menu_item->menu_item_parent ];
					}

					// Make sure we have an object ID
					if ( ! isset( $parent_menu_item->object_id ) ) {
						break;
					}

					// Store the section ID in the cache
					$menu_section_post_id = $parent_menu_item->object_id;

				} // If item is a parent
				else {

					// Store this item's object
					$menu_section_post_id = $menu_item->object_id;

				}

			}

		}
	}

	// If we have a menu section post ID, store it in the cache
	if ( $menu_section_post_id > 0 ) {
		wp_cache_set( 'menu_section_post_id', $menu_section_post_id, 'sa_framework' );
	}

	return $sorted_menu_items;
}, 100, 2 );

// Filter the menu to react on "second_level_menu" flag
add_filter( 'wp_nav_menu_objects', function( $sorted_menu_items, $args ) {

	// If we're not wanting the submenu, we don't need to run this filter
	if ( ! ( isset( $args->second_level_menu ) && $args->second_level_menu ) ) {
		return $sorted_menu_items;
	}

	// This means we're wanting the sub menu affiliated with the current page
	else {

		$submenu_root_id = 0;

		// Find the current menu item
		foreach ( $sorted_menu_items as $menu_item ) {

			if ( $menu_item->current ) {

				// Set the root id based on whether the current menu item has a parent or not
				if ( $menu_item->menu_item_parent ) {
					$submenu_root_id = $menu_item->menu_item_parent;
				} else {
					$submenu_root_id = $menu_item->ID;
				}

				break;

			}

		}

		// Find the top level parent
		if ( ! isset( $args->direct_parent ) ) {

			$prev_root_id = $submenu_root_id;

			while ( $prev_root_id != 0 ) {

				foreach ( $sorted_menu_items as $menu_item ) {

					if ( $menu_item->ID == $prev_root_id ) {

						$prev_root_id = $menu_item->menu_item_parent;

						// Don't set the root_id to 0 if we've reached the top of the menu
						if ( $prev_root_id != 0 ) {
							$submenu_root_id = $menu_item->menu_item_parent;
						}

						break;

					}

				}

			}

		}

		$menu_item_parents = array();

		foreach ( $sorted_menu_items as $key => $item ) {

			// Init menu_item_parents
			if ( $item->ID == $submenu_root_id )
				$menu_item_parents[] = $item->ID;

			if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {

				// Part of sub-tree: keep!
				$menu_item_parents[] = $item->ID;

			} else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {

				// Not part of sub-tree: away with it!
				unset( $sorted_menu_items[$key] );

			}

		}

		return $sorted_menu_items;

	}

}, 20, 2 );