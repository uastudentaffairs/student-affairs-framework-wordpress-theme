<?php

		// Close the main area if we started it
		if ( has_action( 'sa_framework_start_main_area' ) ) {
		    do_action( 'sa_framework_close_main_area' );
		}

		// Run code after the main area
		do_action( 'sa_framework_after_main_area' );

		// Close the off canvas main menu
		do_action( 'sa_framework_close_off_canvas_main_menu' );

		?></div> <!-- #sa-inside-wrapper --><?php

	do_action( 'sa_framework_before_footer' );

	// JS will load the global footer
	?><div id="sa-global-footer"></div><?php

	do_action( 'sa_framework_after_footer' );

	?></div> <!-- #sa-wrapper --><?php

    wp_footer();

?></body>
</html>