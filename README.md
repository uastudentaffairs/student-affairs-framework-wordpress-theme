# UA Student Affairs - Framework - WordPress Theme #

This WordPress theme framework is for [The University of Alabama Division of Student Affairs](https://sa.ua.edu/).

While the design is intended solely for Student Affairs, the code is shared for educational purposes. Feel free to explore, copy, modify, and share the code.

If you have any questions or comments, contact the author: [Rachel Carden](mailto:rmcarden@ua.edu).

### A few facts about this repository: ###

* It was created for [The University of Alabama Division of Student Affairs](https://sa.ua.edu/).
* It was built for use with the [WordPress](https://wordpress.org/) content management system.
* The front-end design is built upon the [Foundation](http://foundation.zurb.com/) framework.

### How do I get set up? ###

* Info coming soon.

### Contribution guidelines ###

* Info coming soon.

### Contact Information ###

* [Rachel Carden](mailto:rmcarden@ua.edu)